/*
 * meli - build.rs
 *
 * Copyright 2020  Manos Pitsidianakis
 *
 * This file is part of meli.
 *
 * meli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * meli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with meli. If not, see <http://www.gnu.org/licenses/>.
 */

extern crate proc_macro;
extern crate quote;
extern crate syn;
mod config_macros;

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    config_macros::override_derive(&[
        ("src/conf/pager.rs", "PagerSettings"),
        ("src/conf/listing.rs", "ListingSettings"),
        ("src/conf/notifications.rs", "NotificationsSettings"),
        ("src/conf/shortcuts.rs", "Shortcuts"),
        ("src/conf/composing.rs", "ComposingSettings"),
        ("src/conf/tags.rs", "TagsSettings"),
        ("src/conf/pgp.rs", "PGPSettings"),
    ]);
    #[cfg(feature = "cli-docs")]
    {
        const MANDOC_OPTS: &[&'static str] = &["-T", "utf8", "-I", "os=Generated by mandoc(1)"];
        use std::env;
        use std::fs::File;
        use std::io::prelude::*;
        use std::path::Path;
        use std::process::Command;
        let out_dir = env::var("OUT_DIR").unwrap();
        let mut out_dir_path = Path::new(&out_dir).to_path_buf();
        out_dir_path.push("meli.txt");

        let output = Command::new("mandoc")
            .args(MANDOC_OPTS)
            .arg("meli.1")
            .output()
            .or_else(|_| Command::new("man").arg("-l").arg("meli.1").output())
            .unwrap();

        let mut file = File::create(&out_dir_path).unwrap();
        file.write_all(&output.stdout).unwrap();
        out_dir_path.pop();

        out_dir_path.push("meli.conf.txt");
        let output = Command::new("mandoc")
            .args(MANDOC_OPTS)
            .arg("meli.conf.5")
            .output()
            .or_else(|_| Command::new("man").arg("-l").arg("meli.conf.5").output())
            .unwrap();
        let mut file = File::create(&out_dir_path).unwrap();
        file.write_all(&output.stdout).unwrap();
        out_dir_path.pop();

        out_dir_path.push("meli-themes.txt");
        let output = Command::new("mandoc")
            .args(MANDOC_OPTS)
            .arg("meli-themes.5")
            .output()
            .or_else(|_| Command::new("man").arg("-l").arg("meli-themes.5").output())
            .unwrap();
        let mut file = File::create(&out_dir_path).unwrap();
        file.write_all(&output.stdout).unwrap();
    }
}
