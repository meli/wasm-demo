# meli WebAssembly demo

This is a fork of `meli` with patches applied on top to make it compile to webassembly targets.

See it live on <https://meli-email.org/wasm2.html>.
