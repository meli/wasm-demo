[package]
name = "melib"
version = "0.6.1"
authors = ["Manos Pitsidianakis <epilys@nessuent.xyz>"]
edition = "2018"
build = "build.rs"

homepage = "https://meli.delivery"
repository = "https://git.meli.delivery/meli/meli.git"
description = "backend mail client library"
keywords = ["mail", "mua", "maildir", "imap"]
categories = [ "email"]
license = "GPL-3.0-or-later"

[lib]
name = "melib"
path = "src/lib.rs"

[dependencies]
bitflags = "1.0"
crossbeam = "0.7.2"
data-encoding = "2.1.1"
encoding = "0.2.33"
memmap = { version = "0.5.2", optional = true }
nom = { version = "5.1.1" }

indexmap = { version = "^1.5", features = ["serde-1", ] }
notify = { version = "4.0.15", optional = true }
xdg = "2.1.0"
native-tls = { version ="0.2.3", optional=true }
serde = { version = "1.0.71", features = ["rc", ] }
serde_derive = "1.0.71"
bincode = "1.2.0"
uuid = { version = "0.8.1", features = ["serde", "v4", "v5", "stdweb", "wasm-bindgen"] }

unicode-segmentation = { version = "1.2.1", optional = true }
libc = {version = "0.2.59", features = ["extra_traits",]}
isahc = { version = "0.9.7", optional = true, default-features = false, features = ["http2", "json", "text-decoding"]}
serde_json = { version = "1.0", optional = true, features = ["raw_value",] }
smallvec = { version = "^1.4.0", features = ["serde", ] }
nix = "0.17.0"
rusqlite = {version = "0.24.0", optional = true }

libloading = { version = "0.6.2", optional = true }
futures = "0.3.5"
#smol = "0.1.18"
async-stream = "0.2.1"
base64 = { version = "0.12.3", optional = true }
flate2 = { version = "1.0.16", optional = true }

wasm-bindgen = { version = "0.2.63", optional = true }

# The `console_error_panic_hook` crate provides better debugging of panics by
# logging them with `console.error`. This is great for development, but requires
# all the `std::fmt` and `std::panicking` infrastructure, so isn't great for
# code size when deploying.
console_error_panic_hook = { version = "0.1.6", optional = true }

# `wee_alloc` is a tiny allocator for wasm that is only ~1K in code size
# compared to the default allocator's ~10K. It is slower than the default
# allocator, however.
#
# Unfortunately, `wee_alloc` requires nightly Rust when targeting wasm for now.
wee_alloc = { version = "0.4.5", optional = true }

[dependencies.js-sys]
version = "0.3.4"
optional = true

[features]
default = ["unicode_algorithms","vcard", "wasm"]

debug-tracing = []
deflate_compression = ["flate2", ]
http = ["isahc"]
http-static = ["isahc", "isahc/static-curl"]
tls = ["native-tls"]
imap_backend = ["tls"]
jmap_backend = ["http", "serde_json"]
maildir_backend = ["notify", "memmap"]
mbox_backend = ["notify", "memmap"]
notmuch_backend = []
smtp = ["tls", "base64"]
sqlite3 = ["rusqlite", ]
unicode_algorithms = ["unicode-segmentation"]
vcard = []
wasm = ["console_error_panic_hook", "wasm-bindgen", "wee_alloc", "js-sys", "deflate_compression"]

[build-dependencies]
flate2 = { version = "1.0.16" }
