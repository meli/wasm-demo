/*
 * meli - bin.rs
 *
 * Copyright 2017-2018 Manos Pitsidianakis
 *
 * This file is part of meli.
 *
 * meli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * meli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with meli. If not, see <http://www.gnu.org/licenses/>.
 */

//!
//!  This crate contains the frontend stuff of the application. The application entry way on
//!  `src/bin.rs` creates an event loop and passes input to a thread.
//!
//! The mail handling stuff is done in the `melib` crate which includes all backend needs. The
//! split is done to theoretically be able to create different frontends with the same innards.
//!

#[allow(unused_variables)]
extern crate wasm_bindgen;

use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
extern crate wee_alloc;
#[cfg(feature = "wee_alloc")]
#[global_allocator]
#[cfg(feature = "wee_alloc")]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

mod helpers {
    use wasm_bindgen::prelude::*;
    #[wasm_bindgen]
    extern "C" {
        #[wasm_bindgen(js_namespace = console)]
        pub fn log(s: &str);
    }

    pub use log as js_console;

    pub static ENRON_CONFIG: &'static str = r#"
[accounts.demo-account]
root_mailbox = "ermis-f"
format = "demo"
index_style = "conversations" # or [plain, threaded, compact]
identity="frank.ermis@enron.com"
display_name = "Frank Ermis"
subscribed_mailboxes = ["*"]
mailboxes."qenron".listing.index_style = "compact"

[composing]
# required for sending e-mail
send_mail = '/bin/true'
    "#;
}

pub use helpers::*;
#[wasm_bindgen]
pub fn greet() {
    js_console("Hello, wasm-game-of-life!");
}

use std::sync::{Arc, Mutex};
//use js_sys::{Array, Date};
//use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::CustomEvent;

use std::collections::VecDeque;
extern crate xdg_utils;
#[macro_use]
extern crate serde_derive;
extern crate linkify;
extern crate uuid;

extern crate bitflags;
extern crate serde_json;
#[macro_use]
extern crate smallvec;

#[macro_use]
extern crate melib;
use melib::*;

#[macro_use]
pub mod types;
use crate::types::*;

#[macro_use]
pub mod terminal;
use crate::terminal::*;

#[macro_use]
pub mod command;
use crate::command::*;

pub mod state;
use crate::state::*;

pub mod components;
use crate::components::*;

#[macro_use]
pub mod conf;
use crate::conf::*;

pub mod pool;
use crate::pool::*;

#[cfg(feature = "sqlite3")]
pub mod sqlite3;

pub mod jobs;
pub mod mailcap;
//pub mod plugins;

//use nix;
//use std::os::raw::c_int;

// Called by our JS entry point to run the example
#[wasm_bindgen(start)]
pub fn run() -> std::result::Result<(), JsValue> {
    extern crate console_error_panic_hook;
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));

    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");
    let body = document.body().expect("document should have a body");

    // Manufacture the element we're gonna append
    let root_element = document.get_element_by_id("terminal").unwrap();
    //root_element.set_inner_html("Loading terminal from inside Rust!");
    if let Some(root_element) = wasm_bindgen::JsCast::dyn_ref::<web_sys::HtmlElement>(&root_element)
    {
        root_element.set_tab_index(1);
    }
    //let (sender, receiver) = crossbeam::channel::bounded(32 * ::std::mem::size_of::<ThreadEvent>());
    let state: Arc<Mutex<Option<State>>> = Arc::new(Mutex::new(None));

    {
        let cb = Closure::wrap(Box::new(move |event: web_sys::Event| {
            js_console("cb focus?");
            let target = event.target();
            if let Some(root_element) = target
                .as_ref()
                .and_then(|target| wasm_bindgen::JsCast::dyn_ref::<web_sys::HtmlElement>(target))
            {
                js_console("focus!");

                js_console(&format!("{:?}", root_element.focus()));
            }
        }) as Box<dyn FnMut(_)>);

        root_element
            .add_event_listener_with_callback("touchend", cb.as_ref().unchecked_ref())
            .unwrap();
        cb.forget(); // TODO cycle collect
    }
    {
        let state = state.clone();
        let cb = Closure::wrap(Box::new(move |event: web_sys::Event| {
            if let Some(key_e) = wasm_bindgen::JsCast::dyn_ref::<web_sys::KeyboardEvent>(&event) {
                let mut state_lck = state.lock().unwrap();
                let state_mut = state_lck.as_mut().unwrap();
                js_console(&format!("key event {:?}", key_e));
                let event = ThreadEvent::Input((Key::from(key_e), vec![]));
                state_event_loop(state_mut, event);
            } else {
                js_console(&format!("not key event {:?}", event));
            }
        }) as Box<dyn FnMut(_)>);

        root_element
            .add_event_listener_with_callback("keydown", cb.as_ref().unchecked_ref())
            .unwrap();
        cb.forget(); // TODO cycle collect
    }
    {
        let state = state.clone();
        let cb = Closure::wrap(Box::new(move |event: web_sys::Event| {
            if let Some(customevent) = wasm_bindgen::JsCast::dyn_ref::<CustomEvent>(&event) {
                if let Some(ptr) = customevent.detail().as_f64() {
                    let b: Box<ThreadEvent> =
                        unsafe { Box::from_raw(ptr as u32 as *mut ThreadEvent) };
                    crate::js_console(&format!("received thread event {:?}", b));
                    let mut state_lck = state.lock().unwrap();
                    let state_mut = state_lck.as_mut().unwrap();
                    state_event_loop(state_mut, *b);
                } else if let Some(s) = customevent.detail().as_string() {
                    if s == "resize" {
                        let b = ThreadEvent::UIEvent(UIEvent::Resize);
                        crate::js_console(&format!("received thread event {:?}", b));
                        let mut state_lck = state.lock().unwrap();
                        let state_mut = state_lck.as_mut().unwrap();
                        state_mut.update_size();
                        state_mut.render();
                        state_mut.redraw();
                    } else if s == "pulse" {
                        //let b = ThreadEvent::Pulse;
                        //crate::js_console(&format!("received thread event {:?}", b));
                        //let mut state_lck = state.lock().unwrap();
                        //let state_mut = state_lck.as_mut().unwrap();
                        //state_mut.redraw();
                    }
                }
            } else {
                js_console(&format!("not thread event {:?}", event));
            }
        }) as Box<dyn FnMut(_)>);

        root_element
            .add_event_listener_with_callback("meli-event", cb.as_ref().unchecked_ref())
            .unwrap();
        cb.forget(); // TODO cycle collect
    }

    let mut state_lck = state.lock().unwrap();
    *state_lck = Some(
        State::new(
            None, //Some(Settings::without_accounts().unwrap_or_default()),
                 //sender, receiver,
        )
        .unwrap(),
    );

    let state_mut = state_lck.as_mut().unwrap();

    let window = Box::new(Tabbed::new(
        vec![
            Box::new(listing::Listing::new(&mut state_mut.context)),
            Box::new(ContactList::new(&state_mut.context)),
            Box::new(StatusPanel::new(crate::conf::value(
                &state_mut.context,
                "theme_default",
            ))),
        ],
        &state_mut.context,
    ));

    let status_bar = Box::new(StatusBar::new(window));
    state_mut.register_component(status_bar);
    state_mut.render();
    state_mut.redraw();
    state_event_loop(state_mut, ThreadEvent::Pulse);
    /*

    {
            let closure: Closure<FnMut(_)> = Closure::new(move |event: web_sys::MouseEvent| {
            context.begin_path();
            context.move_to(event.offset_x() as f64, event.offset_y() as f64);
            pressed.set(true);
        });
        (canvas.as_ref() as &web_sys::EventTarget)
            .add_event_listener_with_callback("mousedown", closure.as_ref().unchecked_ref())
            .unwrap();
        closure.forget();
    }
    */
    Ok(())
}

fn state_event_loop(state: &mut State, event: ThreadEvent) {
    let mut event_opt = Some(event);
    while let Some(event) = event_opt.take() {
        match event {
            ThreadEvent::Input((k, _)) => {
                match state.mode {
                    UIMode::Normal => match k {
                        Key::Char('q') | Key::Char('Q') => {
                            state.context.replies.push_back(UIEvent::StatusEvent(
                                StatusEvent::DisplayMessage(
                                    "Can't quit. Just close the browser tab.".to_string(),
                                ),
                            ));
                            state.redraw();
                        }
                        Key::Char(' ') => {
                            state.mode = UIMode::Command;
                            state.rcv_event(UIEvent::ChangeMode(UIMode::Command));
                            state.redraw();
                        }
                        key => {
                            state.rcv_event(UIEvent::Input(key));
                            state.redraw();
                        }
                    },
                    UIMode::Insert => match k {
                        Key::Char('\n') | Key::Esc => {
                            state.mode = UIMode::Normal;
                            state.rcv_event(UIEvent::ChangeMode(UIMode::Normal));
                            state.redraw();
                        }
                        k => {
                            state.rcv_event(UIEvent::InsertInput(k));
                            state.redraw();
                        }
                    },
                    UIMode::Command => match k {
                        Key::Char('\n') | Key::Esc => {
                            state.mode = UIMode::Normal;
                            state.rcv_event(UIEvent::ChangeMode(UIMode::Normal));
                            state.redraw();
                        }
                        k => {
                            state.rcv_event(UIEvent::CmdInput(k));
                            state.redraw();
                        }
                    },
                    UIMode::Embed => {
                        state.rcv_event(UIEvent::EmbedInput((k, vec![])));
                        state.redraw();
                    }
                    UIMode::Fork => {
                        unreachable!("Should not fork in WASM");
                    }
                }
                let events: smallvec::SmallVec<[UIEvent; 8]> = state.context.replies();
                if !events.is_empty() {
                    for e in events {
                        state.rcv_event(e);
                    }
                    state.redraw();
                }
            }
            ThreadEvent::UIEvent(UIEvent::ChangeMode(f)) => {
                state.mode = f;
            }
            ThreadEvent::UIEvent(e) => {
                state.rcv_event(e);
                state.redraw();
                let events: smallvec::SmallVec<[UIEvent; 8]> = state.context.replies();
                if !events.is_empty() {
                    for e in events {
                        state.rcv_event(e);
                    }
                    state.redraw();
                }
            }
            ThreadEvent::JobFinished(job_id) => {
                for account in state.context.accounts.values_mut() {
                    if account.process_event(&job_id) {
                        break;
                    }
                }
            }
            other => {
                js_console(&format!("unused thread event {:?}", other));
            }
        }
        {
            let mut event_queue_lck = state.context.sender.event_queue.lock().unwrap();
            event_opt = event_queue_lck.pop_back();
        }
        'replies_loop: loop {
            let events: smallvec::SmallVec<[UIEvent; 8]> = state.context.replies();
            debug!("replies: {:?}", &events);
            if events.is_empty() {
                break 'replies_loop;
            }
            for e in events {
                state.rcv_event(e);
            }
            state.redraw();
        }
    }
}
