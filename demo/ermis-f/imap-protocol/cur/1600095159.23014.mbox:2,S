MBOX-Line: From MRC at CAC.Washington.EDU  Tue Apr 11 14:25:39 2006
To: imap-protocol@u.washington.edu
From: Mark Crispin <MRC@CAC.Washington.EDU>
Date: Fri Jun  8 12:34:37 2018
Subject: [Imap-protocol] LIST Clarification
In-Reply-To: <syMWpRdWCviH+GmTLGxvWQ.md5@libertango.oryx.com>
References: <443A7A2D.2070708@consilient.com> <web-35034698@mail.stalker.com>
	<Pine.OSX.4.64.0604101053530.2906@pangtzu.panda.com>
	<web-35035906@mail.stalker.com>
	<Pine.WNT.4.65.0604101441160.4904@Tomobiki-Cho.CAC.Washington.EDU> 
	<443C0286.60200@att.com>
	<Pine.WNT.4.65.0604111228310.3332@Tomobiki-Cho.CAC.Washington.EDU>
	<syMWpRdWCviH+GmTLGxvWQ.md5@libertango.oryx.com>
Message-ID: <Pine.WNT.4.65.0604111418130.3332@Tomobiki-Cho.CAC.Washington.EDU>

On Tue, 11 Apr 2006, Arnt Gulbrandsen wrote:
>>> Our system is case sensitive. We treat 1 and 2 as the same mailbox,
>>> and 3 and 4 as the same mailbox.
>> Of course, this is the sensible interpretation.  However, are we
>> certain that there is nobody who is nonsensical enough to treat these
>> as four different names?
> I naively wrote code implementing the RFC as written, and discovered
> that the code treated all four as different. Rather surprised me. I
> would not bet that everyone else has avoided the mistake.

I don't think that the RFC "as written" requires that all four be treated 
as different.  Rather, the RFC simply doesn't take a position.

To be honest, I would have preferred to prohibit case-sensitivity in 
mailbox names.  However, at the time there was a lot of pushback from the 
UNIX community as to why case-sensitivity was so critically important for 
the modern UNIX world, and how the future would be case-sensitive forever.

Today, I note that Mac OS X's default filesystem is case-insensitive (as 
is Windows XP).  I actually used it for several months before I realized 
that it was case-insensitive; and if I hadn't tried to unpack a tarball 
with two files that differed only by case, I probably would still not have 
noticed.

I have this lingering nightmare that the day will come that I will be 
flamed for case-sensitivity in mailbox names, and being too stupid to 
recognize the obvious benefits of case-insensitivity (remember that I came 
from the PDP-10 world...).  *Shudder*

-- Mark --

http://staff.washington.edu/mrc
Science does not emerge from voting, party politics, or public debate.
Si vis pacem, para bellum.

