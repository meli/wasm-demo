MBOX-Line: From dave at cridland.net  Tue Oct  5 05:24:48 2010
To: imap-protocol@u.washington.edu
From: Dave Cridland <dave@cridland.net>
Date: Fri Jun  8 12:34:45 2018
Subject: [Imap-protocol] Fwd: iOS IMAP IDLE (Standard "Push Email")
	Deficiency, Explanation?
In-Reply-To: <87lIcMeyz6O3xDzKeX7qvQ.md5@[192.168.16.40]>
References: <87lIcMeyz6O3xDzKeX7qvQ.md5@[192.168.16.40]>
Message-ID: <11166.1286281488.738798@puncture>

On Tue Oct  5 11:29:21 2010, Arnt Gulbrandsen wrote:
> Dave Cridland writes:
>> The "3G session" does need to stay open, but it can stay in Idle,  
>> or  PCH, modes. These cost in the region of 8mA.
>> 
>> Small notifications - including the EXISTS and FETCH FLAGS that  
>> IDLE  typically emits from the server - will only raise the 3G  
>> session to  FACH mode. If the handset is forced into FACH mode  
>> constantly,  this'll drain the battery in around 7 hours, using  
>> around 140mA.
>> 
>> Only if the data size reaches a certain (small) threshold - about  
>> 128  octets typically - will the radio rise to DCH mode, where the  
>> drain  is around 380mA - sufficient to drain the battery in three  
>> hours.
> 
> Thanks.
> 
> IIRC iphone mail 4.x does implement idle. It also implements  
> compress and condstore, both of which ought to help avoid DCH.  
> (Those 128 bytes would be about 80 after TCP/IP overhead, or  
> 100-200 with IMAP or TLS compression, right?)

Not according to  
http://developer.apple.com/library/ios/#releasenotes/General/RN-iPhoneSDK-4_0/index.html

    * Mail now supports the following RFC extensions:
          o COMPRESS (4978)
          o ESEARCH (4731)
          o CHUNKING (3030)
          o 8BITMIME (1652)
          o ENHANCEDSTATUSCODES (3463)
          o BINARYMIME (3030)
          o CONDSTORE (4551)

So no QRESYNC, which is weird, and no IDLE.

And yes, those ~128 octets include TCP and TLS, so you drop by (if  
memory serves) about 57 octets. Quite what that buys you in terms of  
compression is anyone's guess, and I'd imagine quite dependent on  
usage patterns.

> 8mA isn't much.

No, it'll let your smartphone last for something over 5 days  
(assuming you've a 1000mAh battery like most of them). Of course,  
unless you have a very long idle timeout and nobody actually sending  
you email, you won't stay in the Idle/PCH states for that long.

But in practise, an IDLE session in typical usage seems to last me  
well over a day on my E71, and it certainly lasts longer than XMPP,  
but I'm working on the latter.

Dave.
-- 
Dave Cridland - mailto:dave@cridland.net - xmpp:dwd@dave.cridland.net
  - acap://acap.dave.cridland.net/byowner/user/dwd/bookmarks/
  - http://dave.cridland.net/
Infotrope Polymer - ACAP, IMAP, ESMTP, and Lemonade

