MBOX-Line: From johngalton217 at gmail.com  Tue Mar 20 14:00:36 2012
To: imap-protocol@u.washington.edu
From: John Galton <johngalton217@gmail.com>
Date: Fri Jun  8 12:34:48 2018
Subject: [Imap-protocol] CONDSTORE mod-sequence values
In-Reply-To: <1370186682.140190.1332276137465.JavaMail.root@zimbra.com>
References: <CABHfyPSq=7hOtH94aet6T=osoXmfVP94iAq6iCCYH3N+MvGmFw@mail.gmail.com>
	<1370186682.140190.1332276137465.JavaMail.root@zimbra.com>
Message-ID: <CABHfyPRH8_FGaDhx6s5rnhk6B=bcuqeoWTm00=6aktBVA-3t5A@mail.gmail.com>

On Tue, Mar 20, 2012 at 1:42 PM, Dan Karp <dkarp@zimbra.com> wrote:

> > From RFC4551 Introduction: "The server MUST guarantee that each STORE
> > command performed on the same mailbox ... will get a different
> > mod-sequence value."
> >
> > Can someone explain why the uniqueness requirement is necessary for
> > the modification sequence? If two metadata items/messages are
> > modified transactionally and share the same mod-sequence I don't
> > really see any way that will break any of the proposed IMAP protocol
> > changes for CONDSTORE (as long as they are updated atomically and a
> > client can't sync between when a first item gets a mod-sequence and
> > a second item gets the same mod-sequence).
>
> I *think* that you're incorrectly interpreting "each STORE command" as
> "each message affected by a STORE command".  The following command is
> one STORE command, not ten:
>
>   A001 STORE 1:10 +FLAGS (\Deleted)
>
> Your single transactional modification of those ten messages is allowed
> to use the same mod-sequence value.
>

Ok that's fine for STORE but what about other actions that may modify
multiple messages at once (say inserting/deleting mails from the mailbox
OOB with IMAP).  Sounds like what really the RFC is getting at is that each
MODSEQ value must be a transactional state, that clients will always see
all changes for a given MODSEQ (i.e. multiple FETCH MODSEQ should always
return the same results, nothing new).

For example, conversely, if that single "STORE 1:10 +FLAGS (\Deleted)" used
the same MODSEQ for all 10 messages and the client could "FETCH MODSEQ"
multiple times while the modification was executing and see new results on
subsequent attempts, that should be disallowed.

Thanks.


>
> - Dan
>
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mailman13.u.washington.edu/pipermail/imap-protocol/attachments/20120320/6300ca38/attachment.html>
