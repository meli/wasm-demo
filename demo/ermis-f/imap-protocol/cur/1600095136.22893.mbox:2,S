MBOX-Line: From dave at cridland.net  Tue Oct 30 03:22:41 2007
To: imap-protocol@u.washington.edu
From: Dave Cridland <dave@cridland.net>
Date: Fri Jun  8 12:34:40 2018
Subject: [Imap-protocol] re: GMail
In-Reply-To: <alpine.WNT.0.9999.0710291616560.3152@Tomobiki-Cho.CAC.Washignton.EDU>
References: <alpine.WNT.0.9999.0710241938220.520@Shimo-Tomobiki.Panda.COM>
	<fc2c80ae0710270911o23f0b366g942b2956ac3fa@mail.gmail.com>
	<alpine.WNT.0.9999.0710270924180.4788@Shimo-Tomobiki.Panda.COM>
	<5202.1193523708.069559@peirce.dave.cridland.net>
	<alpine.OSX.0.9999.0710271708460.11181@pangtzu.panda.com>
	<1193531029.25921.561.camel@hurina>
	<alpine.WNT.0.9999.0710271748490.204456@Ningyo-no-Mori.Panda.COM>
	<RMRLrFbL0dA92HCVkk6Cgw.md5@libertango.oryx.com>
	<alpine.OSX.0.9999.0710280923190.11181@pangtzu.panda.com>
	<d1klUHoFkFbXOtQmbn1Bww.md5@libertango.oryx.com>
	<47263322.7040702@psaux.com> <1193689139.25921.618.camel@hurina>
	<472653EE.9080102@psaux.com>
	<alpine.WNT.0.9999.0710291450130.3152@Tomobiki-Cho.CAC.Washignton.EDU>
	<47266943.2010409@psaux.com>
	<alpine.WNT.0.9999.0710291616560.3152@Tomobiki-Cho.CAC.Washignton.EDU>
Message-ID: <1040.1193739761.374080@peirce.dave.cridland.net>

On Mon Oct 29 23:34:29 2007, Mark Crispin wrote:
> On Mon, 29 Oct 2007, Tim Showalter wrote:
>> I withdraw the comment.  (I made it based on something I read on  
>> this list some number of years ago, and I've never read the IMAP3  
>> document. Good thing, too. :-) )
> 
> It's instructive to read the IMAP3 document (RFC 1203), if only to  
> see a dead-end branch in IMAP's evolution.
> 
> RFC 1176 was a very minor update from RFC 1064, basically adding  
> the FIND command (a nascent form of LIST).  Much of RFC 1203 is a  
> strong reaction against the IMAP architecture (complete with a  
> diatribe to the effect that IMAP will die unless IMAP3 is adopted),  
> especially the unsolicited data model (e.g., it tags FETCH  
> responses).
> 
> 
I think that tagged intermediate responses can be useful, as it  
happens, but not for FETCH - this does, as you say, break the data  
model. They'd have been useful for SEARCH, SORT and THREAD, though.

>> What's the real difference between having strictly-increasing  
>> client capabilities, or revising the base spec?  In both cases,  
>> we're bundling some arbitrary set of protocol changes and  
>> requiring client/server support for all of them.
> 
> I think that there is much more opposition to adding to a base  
> specification than to a service level.  A new base specification  
> purported kills the previous one; whereas a service level might get  
> ignored at a particular point.
> 
> Sometimes how you call things does matter!
> 
> 
Calling things a Profile can work too.


>> Thinking about this a little more, I think there's an argument to  
>> be made for limiting capabilities and trying to decrease the  
>> number of different available capability sets, but we probably  
>> also need some amount of forking near the cutting-edge of protocol.
> 
> I agree completely!
> 
> 
As do I, but I'm wary of discarding extensions as well. Extensions  
can be developed incrementally, whereas profiles, service levels, etc  
take a lot more effort.


>> But if UTF8 is the first one, and it's successful and widely  
>> adopted, whatever the next wave is, should require UTF8.
> 
> I think that upgrading IMAP for UTF-8 is essential, and was  
> foreseen with the publication of RFC 2060.
> 
> 
Mostly foreseen - certainly the lack of any encoding for keywords has  
bitten us. Otherwise, we have a relatively clear upgrade path, and  
EAI has taken advantage of some of this.


>> But I'm very concerned about the complexity here.  It's apparently  
>> hard to implement correct IMAP clients and servers now.
> 
> I agree completely!
> 
> 
I don't think it's hard to implement a client correctly; although I  
do think it'd hard to take full advantage of the protocol. But this  
is largely a result of the lack of design documentation in the later  
IMAP specifications. Reading the earlier ones, whilst they're not as  
polished as the later specifications, was very useful to me. (In  
particular, the unsolicited data model is not adequately explained in  
RFC3501, but is better explained in RFC1176 and RFC1064 - the fourth  
paragraph of RFC1064's "The Protocol" section ought to be required  
reading.).


>> I'm not at all convinced of the wisdom of any of these approaches,  
>> but I am enjoying the discussion.
> 
> I think that we are all groping towards something that will address  
> the problem without causing the Law of Unintended Consequences to  
> come down hard upon us... ;-)

Certainly anything we do requires a significant amount of thought.

Dave.
-- 
Dave Cridland - mailto:dave@cridland.net - xmpp:dwd@jabber.org
  - acap://acap.dave.cridland.net/byowner/user/dwd/bookmarks/
  - http://dave.cridland.net/
Infotrope Polymer - ACAP, IMAP, ESMTP, and Lemonade

