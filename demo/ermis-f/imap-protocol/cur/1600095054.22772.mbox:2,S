MBOX-Line: From bsmcconnell at gmail.com  Mon May  2 14:42:31 2011
To: imap-protocol@u.washington.edu
From: Brian McConnell <bsmcconnell@gmail.com>
Date: Fri Jun  8 12:34:45 2018
Subject: [Imap-protocol] Looking for help implementing an IMAP-to-HTTP
	gateway for web app developers
Message-ID: <7D2A9AA5-CC8F-4C8A-A917-4E242E7BCE12@gmail.com>

Hello,

I am working on an interesting project, and am looking for people who are knowledgeable about IMAP server implementations to join us. 

I wrote an article, which you can find at http://imap2http.wordpress.com (to be published on one of the tech sites in the near future). The article describes a way to use IMAP as a delivery mechanism for mobile applications that provide continually updated information or feeds to users. 

The idea is quite simple, to make a web service look like an IMAP server that a mobile email client can subscribe to. The IMAP-HTTP gateway presents an IMAP interface to the email client, which thinks it is talking to a mail server, and converts IMAP requests into RESTful HTTP queries to the upstream web service. 

This will be a simple way for web app developers to create mobile versions of their service, by using IMAP to deliver information to the mobile device. The logic is that IMAP clients are baked into most devices, run in the background as a core service, and have been thoroughly tested. It is a bit of a hack, but it makes use of something that is already there.

The only missing piece is a reliable IMAP-HTTP utility. This need not be an especially complex program, since it just speaks IMAP to the client, and translates IMAP commands (e.g. a request to fetch a list of folders) into HTTP requests that are submitted to an upstream service, which replies with a formatted response (e.g. a JSON recordset) that, in turn, is converted back into IMAP form. 

I wanted to inquire and see if someone has already built this, or if not, if someone out there would be interested in collaborating on this as an open source project to be made available to web and mobile developers.

Thanks for your time.

Brian McConnell
