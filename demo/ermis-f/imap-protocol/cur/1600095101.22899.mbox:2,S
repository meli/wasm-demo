MBOX-Line: From dkarp at zimbra.com  Thu Aug  2 10:52:10 2007
To: imap-protocol@u.washington.edu
From: Dan Karp <dkarp@zimbra.com>
Date: Fri Jun  8 12:34:40 2018
Subject: [Imap-protocol] NAMESPACE and LIST/LSUB wildcards
Message-ID: <1274724802.313211186077130557.JavaMail.root@dogfood.zimbra.com>

If an IMAP server supports NAMESPACE and defines "other users' namespaces" and/or "shared namespaces" prefixes, should mailboxes beneath these prefix roots be included or omitted from wildcarded LIST/LSUB requests?

For example, if

      C: A001 NAMESPACE
      S: * NAMESPACE (("" "/")) NIL (("Public Folders/" "/"))
      S: A001 OK NAMESPACE command completed

and

      C: A002 SUBSCRIBE "Public Folders/client1"
      S: A002 OK SUBSCRIBE command completed

then should "Public Folders/client1" be returned in either of these?

      C: A003 LIST "" "*"

      C: A004 LSUB "" "*"

My guess is that the answer is "no", but I'm just extrapolating from this bit in RFC 2342 section 5:

   A client can construct a LIST command by appending a "%" to the Other
   Users' Namespace prefix to discover the Personal Namespaces of other
   users that are available to the currently authenticated user.

   A server MAY return a LIST response containing only the names of
   users that have explicitly granted access to the user in question.

   Alternatively, a server MAY return NO to such a LIST command,
   requiring that a user name be included with the Other Users'
   Namespace prefix before listing any other user's mailboxes.


