MBOX-Line: From blong at google.com  Fri Oct 13 00:19:59 2017
To: imap-protocol@u.washington.edu
From: Brandon Long <blong@google.com>
Date: Fri Jun  8 12:34:55 2018
Subject: [Imap-protocol] Is server re-use of UID OK?
In-Reply-To: <b60bb28d-a16d-397d-784b-830265501279@chartertn.net>
References: <38137c2b-f1f1-2bed-e22f-2aea7fa50ac3@chartertn.net>
	<CAByav=gBnVkLg+4z90ewBvKRVtOrEQ7XESfirEQ1dyx=Sb0MXw@mail.gmail.com>
	<abb75221-a38a-3317-86a2-98a8340e55d3@chartertn.net>
	<CAByav=gfGNd2KHyx8kb9GQ-XEfs3L0LzqJuQGJRwDXg9x9mdMA@mail.gmail.com>
	<1a206274-8bce-f789-4dc9-638ed20e9372@chartertn.net>
	<d0bd4b95-90bb-4fae-937e-f4ad0c2ebd09@gulbrandsen.priv.no>
	<CABa8R6ucJXxUp9kAQ+_fDvtEAwmAYtJ6Hmr60J5=SW48SLvmig@mail.gmail.com>
	<b60bb28d-a16d-397d-784b-830265501279@chartertn.net>
Message-ID: <CABa8R6sMcLPOF2gbv-EYAOzwVMVi5CwZC_GZ2aHmuu2xFpovGw@mail.gmail.com>

On Oct 12, 2017 9:46 PM, "Gene Smith" <gds@chartertn.net> wrote:

On 10/9/17 7:30 PM, Brandon Long wrote:


>
> On Mon, Oct 9, 2017 at 7:39 AM, Arnt Gulbrandsen <arnt@gulbrandsen.priv.no
> <mailto:arnt@gulbrandsen.priv.no>> wrote:
>
>     Gene Smith writes:
>
>         Yes, for other IMAP servers I have tested, if I copy message "A"
>         10 times into Inbox I see 10 identical copies of message A in
>         Inbox. For this openwave server, I see only 1 (and uid fetch 1:*
>         (FLAGS) shows only 1).
>
>
>     Have you tested with gmail?
>
>
> Since Gmail considers a COPY operation as an "add label" operation, it's
> usually a no-op if the message already has that label, which is what would
> happen in this case.
>
> There are cases where Gmail does "reassign UID" on a COPY, ie it bumps the
> UID of a message to the UIDNEXT.  It does this if the user is calling COPY
> with a source/target folder that are the same, on the assumption that the
> user is trying to make an actual copy of the message in the same folder.
> Clients doing this tend to be confused if the message isn't the latest UID
> in that case.  We also reassign UIDs when messages are moved to/from the
> spam/trash folders, for similar reasons.  (Note that APPEND always bumps
> the UID when it detects an attempt to APPEND a duplicate message for
> similar reasons to the COPY to the same folder)
>
> Most IMAP flags are also implemented as labels, and so are consistent
> across all "copies" of a message.  The exception is \Deleted, so doing
> something like a COPY/Deleted as a MOVE operation without an EXPUNGE and
> then doing it back will leave both "copies" with a \Deleted flag and
>

To simulate a MOVE, wouldn't you COPY the message to a destination folder
and then set the \Deleted on the source message/folder. So the \deleted
flag is not on both copies? Then you would uid expunge the source message.


both will be removed with EXPUNGE.  That said, the default is for the user
> to be in auto-expunge mode, in which case storing a \Deleted flag will
> result in the message being EXPUNGED (that label removed) at the next sync
> point.
>
> IE, from the given example:
> C: aaa UID COPY 1267 "Mbox"
> S: aaa OK [COPYUID 123456789 1267 1007] UID COPY completed
> C: bbb UID store 1267 +Flags (\Deleted)
>
> The UID store is a sync point, so the message will be auto-expunged at
> this point, so the COPY back will get a new UID.
>

Thanks for the info on gmail. One thing I didn't understand is why gmail
has the "auto-expunge on \Deleted" default feature. You have provide a
rationale for it. But it seems to be a violation of rfc 3501. It also
causes problem for clients that use the "Just mark it as deleted" delete
method that just sets the \Deleted flag and leaves a deleted email summary
in place but indicates it is deleted by drawing a line though the summary.
With default gmail, the crossed-out message vanishes from the summary list
since it auto-expunged. This can be fixed in the gmail imap settings,
fortunately. The outlook imap server also does the auto-expunge but doesn't
seem to have a way to disable it. I haven't yet seen other imap servers
that do auto-expunge.


What is the utility of the leave in folder marked as deleted?

Most clients moved to a trash folder concept, which doesn't require the
leave deleted state.

Gmail added auto-expunge as soon as we started testing imap, since people
would delete messages... And they'd still be there in the web interface.
We had no interest in adding the weird deleted still there mode to the
Gmail client.  We debated a couple different modes, and they basically
wound up as advanced settings, but just expunging made the most sense,
especially since most mobile clients at the time never called expunge, and
the main use case of imap for Gmail was for mobile clients.

It is not a violation of RFC 3501, since another client could be connected
and issue an expunge, as long as it's only visible at a sync point.

It did cause the occasional issue with clients that couldn't handle it in
all cases, but those were all client bugs.


> You'd need to change the user using the advanced IMAP options to a
> different deletion mode to see the UID maintained.
>

Advanced IMAP option == not auto-expunge? I think that's what you mean.

OK, if I do this with auto-expunge set to false:
Copy message "test" (uid 5604) from Inbox to [Gmail]/f1
Message test is visible in Inbox and f1. Not crossed-out.
In Inbox, mark test with \deleted. It becomes crossed-out.
In f1 copy test to Inbox. Goes to uid 5604 in Inbox.
test in Inbox keeps \Deleted flag and remain crossed-out.

So gmail imap behaving same as openwave imap! It copies back to the
original UID and doesn't clear the \Deleted flag on UID 5604 in Inbox and
message remains crossed-out in client.

Why doesn't \Deleted on UID 5604 in Inbox get cleared after copy back?


What is the proper thing to do?  We're mixing two mailbox models which
aren't entirely compatible.

I agree that clearing the flag is probably the right choice.  Most likely,
this is a fairly unlikely scenario that only occurs with a really small
number of clients when a very unused setting is enabled, so no one ran into
it and made a decision.  I could probably file a bug for it.

Brandon


> Also, Gmail offers the MOVE extension, so you should really use that
> instead of COPY\Deleted\EXPUNGE.
>

Yes, no problem when MOVE used since it expunges the message after it
copies it to the destination. That's typically what is used.


> Brandon
>
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mailman13.u.washington.edu/pipermail/imap-protocol/attachments/20171013/00ca0530/attachment.html>
