MBOX-Line: From jkt at flaska.net  Tue Mar 18 14:58:35 2014
To: imap-protocol@u.washington.edu
From: =?iso-8859-1?Q?Jan_Kundr=E1t?= <jkt@flaska.net>
Date: Fri Jun  8 12:34:52 2018
Subject: [Imap-protocol] STARTTLS after PREAUTH
In-Reply-To: <20140318152549.Horde.0C2tXb4vwx_29xt0ZbwdEQ4@bigworm.curecanti.org>
References: <20140318141305.Horde.iyy0UP8Ostx9TojRZiFyjw1@bigworm.curecanti.org>
	<059bac1f-35eb-4f87-bd5e-e986dfb46b83@flaska.net>
	<20140318152549.Horde.0C2tXb4vwx_29xt0ZbwdEQ4@bigworm.curecanti.org>
Message-ID: <c6c8113d-7679-49a8-9d3b-408046e53786@flaska.net>

On Tuesday, 18 March 2014 22:25:49 CEST, Michael M Slusarz wrote:
> Except as defined in base spec, STARTTLS is defined as "you are 
> allowed to protect privacy ONLY if you protect authentication".  
> In the absence of a need to protect authentication, you can't 
> protect privacy.

That's right. To my understanding, providing just these two options:

a) don't protect anything,
b) protect everything,

is much simpler than doing this in a more fine-grained manner, and the cost 
of forcing STARTTLS to happen earlier is negligible. But perhaps I'm 
missing something?

>> That's what you could get with AUTH EXTERNAL.
>
> Not a part of the base IMAP spec, which is what I was talking about.

The way I see it, IMAP delegates all means of authentication and 
authorization except the basic LOGIN (which is probably specified mainly 
for backward compatibility) to SASL, doesn't it? But you're of course right 
that it is not a mandatory part of the spec.

Besides, there's a reference to the EXTERNAL and an example of how to use 
it with STARTTLS right in the official IMAP RFC [1].

> Simple real-world example would be a machine on an internal 
> network that is guaranteed to belong to a certain user.

While I can imagine a scenario where an attacker can listen, but cannot 
spoof, I think that would be quite an artificial setup to be honest.

With kind regards,
Jan

[1] http://tools.ietf.org/html/rfc3501#section-6.2.1

-- 
Trojit?, a fast Qt IMAP e-mail client -- http://trojita.flaska.net/

