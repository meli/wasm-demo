MBOX-Line: From oleung at traversenetworks.com  Tue Sep 27 10:45:09 2005
To: imap-protocol@u.washington.edu
From: Otto Leung <oleung@traversenetworks.com>
Date: Fri Jun  8 12:34:36 2018
Subject: [Imap-protocol] partial fetch
Message-ID: <3D9D02497026344DB100E7D18A6C06E3B5E1F5@nbexch01.neubond.com>

Thanks for all the replies.

The reason I asked is that the client software is having problems with
partial fetch.  It hangs when it tries to get an attachment that's
bigger than the chunk size.

The imap server I am connecting to is giving me:

When I send:
A22 FETCH 15 (BODY[1]<245760.16384>)

I am getting:
* 15 FETCH (BODY[1]<245760> {16384}
[some binary data...]
A22 OK FETCH completed

The reason it hangs seems to be the IMAP server did not send 16384 bytes
as it specified {16834}.

I turned off the partial fetch property when I connected to the same
IMAP server, and I could get the attachment successfully.  It appears to
me that the partial fetch implement has a bug on the server side. 

-----Original Message-----
From: mrc@ndcms.cac.washington.edu [mailto:mrc@ndcms.cac.washington.edu]
On Behalf Of Mark Crispin
Sent: Tuesday, September 27, 2005 10:13 AM
To: Otto Leung
Cc: imap-protocol@u.washington.edu
Subject: Re: [Imap-protocol] partial fetch

On Tue, 27 Sep 2005, Otto Leung wrote:
> Why is Partial Fetch important to be implemented on the server side?

It is documented as mandatory-to-implement in the specification. 
Consequently, there are clients which depend upon it being implemented, 
and these clients will not function if it is not implemented.

An IMAP server implementation MUST implement all mandatory-to-implement 
facilities in the base specification.  Unless something is explicitly 
documented as optional (or "MAY") in the base specification, then it is 
mandatory-to-implement.

In general, the only optional facilities in the IMAP base specification 
are the ability to reference non-INBOX mailboxes (the server can reply
NO 
to some other mailbox name) and the ability to SEARCH character sets
other 
than US-ASCII and UTF-8.

-- Mark --

http://staff.washington.edu/mrc
Science does not emerge from voting, party politics, or public debate.
Si vis pacem, para bellum.


