MBOX-Line: From tss at iki.fi  Tue Dec 29 11:17:30 2009
To: imap-protocol@u.washington.edu
From: Timo Sirainen <tss@iki.fi>
Date: Fri Jun  8 12:34:43 2018
Subject: [Imap-protocol] Childless noselect mailboxes
In-Reply-To: <alpine.OSX.2.00.0912291042070.18442@hsinghsing.panda.com>
References: <1262106049.26478.745.camel@timo-desktop>
	<alpine.OSX.2.00.0912290938230.18442@hsinghsing.panda.com>
	<1262111858.26478.842.camel@timo-desktop>
	<alpine.OSX.2.00.0912291042070.18442@hsinghsing.panda.com>
Message-ID: <1262114250.26478.864.camel@timo-desktop>

On Tue, 2009-12-29 at 11:02 -0800, Mark Crispin wrote:
> On Tue, 29 Dec 2009, Timo Sirainen wrote:
> > Dovecot with Maildir always behaved that way, but now that I'm creating
> > a new mailbox format I thought I'd get rid of that restriction. But that
> > in turn seems to be causing more trouble than I thought.
> 
> What, precisely, is the behavior that you purpose to do in these cases:
> 
> [1] "tag CREATE foo/"

Same as "CREATE foo".

> [2] "tag DELETE foo" where foo is selectable and has a child?

Leave foo as \Noselect, don't delete children.

> [3] "tag DELETE foo/bar" where foo is \NoSelect and bar is the only 
> child?

Delete both foo/bar and foo.

> The requirements of IMAP in each case:
..
> As long as you comply with these requirements, you are fine.  

Right.

> If a client wants to keep "empty directories" around, it MUST NOT use the 
> implicit creation feature, and MUST NOT delete mailboxes with children 
> (if it wants to remove messages, use delete+expunge instead).  Put another 
> way, the ONLY way it should make a directory is with the "tag CREATE foo/" 
> form.  If that form makes a \NoSelect name, fine; but if it doesn't, then 
> don't force it.

I think my main question is: When server supports dual-use mailboxes, is
it a good idea for it to not support creation of \Noselect mailboxes
with "CREATE foo/"? If the server was internally capable of either
creating it as \Noselect or selectable, which one would be better? I
think there are two issues:

1) Technically it's more featureful to create it as \Noselect.

2) Users seem to be confused by \Noselect mailboxes.

I'm beginning to think 2) outweighs 1), especially if there's no good
use case for 1).
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 204 bytes
Desc: This is a digitally signed message part
URL: <http://mailman13.u.washington.edu/pipermail/imap-protocol/attachments/20091229/a09cc0c0/attachment.sig>
