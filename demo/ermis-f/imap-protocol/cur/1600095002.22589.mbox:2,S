MBOX-Line: From bill.shannon at ymail.com  Fri May 22 16:46:26 2015
To: imap-protocol@u.washington.edu
From: Bill Shannon <bill.shannon@ymail.com>
Date: Fri Jun  8 12:34:55 2018
Subject: [Imap-protocol] COPYUID message order
Message-ID: <555FBFD2.1030800@ymail.com>

I'm unclear on the requirements around the COPY command and the COPYUID
response code.

Is there any guarantee that the order of messages in the COPY command is
the order they'll appear in the destination mailbox?  Or are they
allowed to appear in any arbitrary order in the destination mailbox?
(After any existing messages, of course.)

RFC 4315 says of the COPYUID response:

      The source UID set is in the order the message(s) were copied; the
      destination UID set corresponds to the source UID set and is in
      the same order.

Exactly what order is that?  Is it the order in which the messages were
mentioned in the COPY command?  Is it the order they actually appear in
the destination mailbox (assuming it can be different)?  Or is it in
some other undefined order?

With Gmail I'm getting

A1 COPY 1:2 dest
A1 OK [COPYUID 123456 1:2 2,1]

and

A1 COPY 2,1 dest
A1 OK [COPYUID 123456 1:2 2,1]

The order of the source UIDs is unrelated to the order the messages
are mentioned in the COPY command and unrelated to the order they
appear in the destination mailbox.

If I don't know the UIDs of the source messages, is there any way to
determine which message was copied to which UID in the destination
mailbox?

