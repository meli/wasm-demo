MBOX-Line: From jkt at flaska.net  Sat Nov 17 03:29:59 2012
To: imap-protocol@u.washington.edu
From: =?iso-8859-1?Q?Jan_Kundr=E1t?= <jkt@flaska.net>
Date: Fri Jun  8 12:34:49 2018
Subject: [Imap-protocol] Re: Suspend/Restore feature proposal
In-Reply-To: <20121116143137.Horde.7Kb3aEW5DhM6CnuA2hAx4Q8@bigworm.curecanti.org>
References: <20121115215854.Horde.zz6B0O0tmt3ylHiEXzXhQQ4@bigworm.curecanti.org>
	<CABa8R6tHP2My0k2LqT1RzHoLQZA+X_jwUMU0cydm2sAwo8f4Fg@mail.gmail.com>
	<20121116143137.Horde.7Kb3aEW5DhM6CnuA2hAx4Q8@bigworm.curecanti.org>
Message-ID: <e122fa25-9110-4b36-855d-0e7e273c5805@flaska.net>

Hi Michael,
I've read your draft, it's an interesting extension. However, it seems to me that the whole point here is to save a few roundtrips by skipping the process of activating/configuring various optional features. I'll discuss each extension separately.

> COMPRESS=DEFLATE

I was wondering if this one actually provides any benefit for a webmail client. But you're right that it indeed has an overhead and requires a full roundtrip to set up. However, please note that your extension also requires a full roundtrip, so you aren't any better here.

> ENABLE (CONDSTORE/QRESYNC)
> LANGUAGE
> COMPARATOR

It looks to me that you can easily pipeline all of these and that you do not risk anything by doing so. Yes, I'm aware of the wording of the ENABLE RFC which sounds like one really MUST check its return code, but a subsequent thread on this list indicated that this was not the desired outcome and that it is completely legal to pipeline ENABLE QRESYNC with SELECT ... QRESYNC.

As of the LANGUAGE -- how often do you expect to hit an error condition which is not described by an appropriate response code? I don't think that blocking for its result would be a good design choice.

And finally, what IMAP servers support the LANGUAGE extension?

> CONVERSIONS
> saved CONTEXTs
> NOTIFY

Are you actually aware of a single IMAP server supporting any of these (besides CONTEXT=SEARCH, which again can easily be pipelined without any race conditions, and is specific to a mailbox state anyway, which is outside of scope of your extensions)?

In general, all of the items which you included as an example look like easily pipelineable items. Have you tried to use pipelining for these? What was the total time spent waiting for their completion in that case? What would be the best theoretical time which you could get by RESTORE?

With kind regards,
Jan

