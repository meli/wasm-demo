MBOX-Line: From tss at iki.fi  Tue Sep  3 16:21:23 2013
To: imap-protocol@u.washington.edu
From: Timo Sirainen <tss@iki.fi>
Date: Fri Jun  8 12:34:51 2018
Subject: [Imap-protocol] Best way to support HTTP PUT based 'push'
	notifications from IMAP?
In-Reply-To: <5225BF8C.7060602@mozilla.com>
References: <5225BF8C.7060602@mozilla.com>
Message-ID: <8B2FFC28-F503-4EAF-90E9-42278ABCBB02@iki.fi>

On 3.9.2013, at 13.53, Andrew Sutherland <asuth@mozilla.com> wrote:

> On devices that either have difficulty maintaining persistent TCP connections or have power concerns about doing so, maintaining an IMAP connection for IDLE/NOTIFY purposes is undesirable and it would be great if the IMAP server could generate a notification via other means.
> 
> For the Firefox OS e-mail client, one of the Web APIs the Firefox OS platform makes available and that Mozilla and other contributors are trying to standardize is the "Push API" (http://www.w3.org/TR/push-api/) under the auspices of the webapps WG (http://www.w3.org/2008/webapps/).  The API is somewhat decoupled from the actual notification mechanism.  Mozilla's reference server protocol is documented at https://wiki.mozilla.org/WebAPI/SimplePush/Protocol.
> 
> From the IMAP server's perspective, the process is that the client uses the Push API to get a URL and then the client tells the IMAP server the URL.  To notify the client, the IMAP server issues a PUT request to the URL.  The only data payload is the 'version' which is a monotonically increasing 64-bit integer and allows for a degree of idempotency.  The only key thing for the version is that if the server doesn't increase the value since the last time the client heard it, the client won't receive a new notification.

Interesting. I talked with Stephan Bosch (Pigeonhole author) about this and here's some of our thoughts:

 - Probably a new IMAP command would be best for this. There would have to be some limit to how many new URLs can be registered.

 - The Protocol Considerations recommends not sending a notification while the client is already connected. This is annoyingly difficult to do, and I don't think it's really worth the effort. (And it would reuqire the logged in client to identify itself anyway somehow.) If a mobile client is already connected, the device's radio is probably already on anyway so it doesn't matter much? And it's going to be sent only once anyway. The client could of course cancel the URL when it logs in.

 - To get widespread adoption the default behavior should be to enable this. I don't really see any dangers in doing that. I'm not sure if by default it should notify only about mails in INBOX, or mails about everywhere except in \Junk or \Trash (as specified by SPECIAL-USE). I'm leaning a bit towards INBOX-only by default.

 - If Sieve script marks the message as \Seen, there shouldn't be a notification

 - Sieve script could be used to further configure which mails/folders trigger a push notification. Most importantly it should be configurable that a high volume mailing list wouldn't trigger a push notification all the time.

 - Most installations don't allow users to directly modify their Sieve scripts. From that perspective specifying the folder push rules via IMAP could be useful (e.g. NOTIFY-based). But then again maybe that's too much complexity, and if this becomes a problem the email providers will do something about it.

 - Push should probably be triggered only by newly delivered mails, not IMAP APPEND/COPY.

 - IMAP clients should re-register the URL periodically even if the previous one hadn't been triggered. Otherwise if a server loses the registered URL for any reason, the client would stop receiving notifications forever.

 - This feature could be quite easily implemented for old Dovecot versions as a plugin, so Debian stable users wouldn't necessarily have to wait 3 years for it.

> ## Hosted IMAP Proxy (Undesirable due to inherent credential/data exposure)
> 
> A proposal from those working on the back-end of Mozilla's push notification service is an HTTPS-accessible proxy.  Instead of having the device maintain the long-lived IMAP connection, the device has a server do that on its behalf.  The very undesirable aspect of such a solution is that while the proxy need not persist the credentials to disk, it does need to know them to establish the connection using traditional LOGIN.  And even if a challenge-response authentication mechanism was used in such a way that the proxy server could not establish additional connections without device support, the proxy (or whatever pretends to be the proxy) would still have a fully authorized IMAP connection to use as it chooses until the connection is dropped.

BTW. Dovecot has the ability to use a remote IMAP server as email storage, so if this push feature was implemented by Dovecot, anyone could get the push functionality from any IMAP server by setting up their own Dovecot proxy.


