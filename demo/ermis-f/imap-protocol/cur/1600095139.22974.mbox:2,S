MBOX-Line: From MRC at CAC.Washington.EDU  Thu Sep 14 18:57:15 2006
To: imap-protocol@u.washington.edu
From: Mark Crispin <MRC@CAC.Washington.EDU>
Date: Fri Jun  8 12:34:37 2018
Subject: [Imap-protocol] reporting/detecting expunged messages
In-Reply-To: <2xKZXsAOqJTXDcZB/xUw7g.md5@libertango.oryx.com>
References: <4505F2FE.8090807@sun.com>
	<Pine.WNT.4.65.0609111648410.5544@Tomobiki-Cho.CAC.Washington.EDU>
	<450713AB.4090601@sun.com>
	<Pine.WNT.4.65.0609121316280.2164@Tomobiki-Cho.CAC.Washington.EDU>
	<45071A57.5080406@sun.com>
	<9SYO8YZNXU3qj9EYGtuGNg.md5@libertango.oryx.com>
	<Pine.WNT.4.65.0609121437140.2164@Tomobiki-Cho.CAC.Washington.EDU>
	<2xKZXsAOqJTXDcZB/xUw7g.md5@libertango.oryx.com>
Message-ID: <Pine.WNT.4.65.0609141841560.1540@Tomobiki-Cho.CAC.Washington.EDU>

On Thu, 14 Sep 2006, Arnt Gulbrandsen wrote:
> Sure. The average case is OK, what I dislike is the lack of a proper 
> transaction. Or you could say I don't like the fuzzy semantics of the EXPUNGE 
> command. When a client issues EXPUNGE and the server issues a tagged OK, what 
> is the server promising the client?

You are looking at it the wrong way.

Instead of looking at it from the protocol viewpoint, you are looking at 
it from the viewpoint of how some server implementation may implement it.

More importantly, you are betraying an unrealistic expectation of what the 
IMAP protocol can do: to wit, that an IMAP client can force destruction of 
a message in other clients.  Doesn't happen.  Can't happen.

The best that can happen is a mutually-cooperative effort; and perhaps an 
entity that refuses to be cooperative can be expelled from the effort.

In IMAP, a message exists in as many as four places:
  (1) the mailbox on the server
  (2) server state in a client/server session
  (3) client state in a client/server session
  (4) client cache of the mailbox

When a client issues an expunge command and gets back an untagged EXPUNGE 
result, then the message has been expunged from (1).  It is also expunged 
from (2) IN THAT SESSION.  If the client implements IMAP correctly, it is 
also expunged from (3).  IMAP has no control over (4), although one would 
presume that a well-implemented client would expunge it in (4) as well.

What is confusing you is (2) and (3) IN OTHER SESSIONS, and (4) in other 
clients; and somehow you have the issue that this renders the semantics of 
EXPUNGE "fuzzy".  The semantics of EXPUNGE are not fuzzy at all; they are 
quite precise.

You need to get it out of your head that the IMAP protocol has any control 
over (4).  There is no way that a forced removal of (4) can ever happen. 
A sufficiently recalictrant client can prevent a forced removal of (3) as 
well, although eventually it'll have to move it to (4).

A server implementation MAY be able to force the issue for (2) in the face 
of a recalcitrant client.  However, the proper way to do that is not by 
pulling the rug from under a well-behaved client that will presently do a 
NOOP or other EXPUNGE-synchronize operation.  A proper way would be a 
forced BYE on a badly-behaved client.

> IMO,  it depends on the precise text. Requiring that all clients offer the 
> server an opportunity to send EXPUNGE every so often can't be all that bad, 
> but how often? "EXPUNGE may not take effect completely until half an hour 
> after the tagged OK" sounds overly long to me. "Clients must give the server 
> the opportunity to send EXPUNGE at two-minute intervals or more often" sounds 
> much too short. Ten minutes sounds both too short and too long. All partly 
> subjective of course.

In my opinion, the minimum period should be between 5 and 30 minutes.  NAT 
has pretty much made it non-viable to go idle for more than 5 minutes.

-- Mark --

http://staff.washington.edu/mrc
Science does not emerge from voting, party politics, or public debate.
Si vis pacem, para bellum.

