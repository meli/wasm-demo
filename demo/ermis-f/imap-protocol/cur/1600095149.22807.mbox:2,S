MBOX-Line: From witold.krecicki at firma.o2.pl  Mon Jun 14 05:31:47 2010
To: imap-protocol@u.washington.edu
From: Witold =?utf-8?q?Kr=C4=99cicki?= <witold.krecicki@firma.o2.pl>
Date: Fri Jun  8 12:34:44 2018
Subject: [Imap-protocol] IMAP MOVE extension
In-Reply-To: <29965.1276517228.230869@puncture>
References: <201006110854.37969.witold.krecicki@firma.o2.pl>
	<201006141248.11054.witold.krecicki@firma.o2.pl>
	<29965.1276517228.230869@puncture>
Message-ID: <201006141431.47814.witold.krecicki@firma.o2.pl>

On Monday 14 of June 2010 14:07:08 Dave Cridland wrote:
> On Mon Jun 14 11:48:11 2010, Witold Kr?cicki wrote:
> > On Monday 14 of June 2010 12:13:57 Dave Cridland wrote:
> > > a) MOVE is simpler for client authors. However, client authors
> > 
> > would
> > 
> > > still have to move messages without this extension, hence have to
> > > provide multiple codepaths to achieve the same - really quite
> > 
> > simple
> > 
> > > - facility.
> > 
> > That's their choice, it's an EXTENSION hence they don't HAVE to use
> > MOVE
> > functionality.
> > Yet MOVE enables user to move messages even if they're at their
> > quota limits,
> > which is currently impossible with enforced quota limits.
> 
> I'm trying to make what I think is quite an important point, so let
> me try and rephrase so you see where I'm coming from.
> 
> If a client implements MOVE, it *still* has to implement a
> move-analogue by COPY/EXPUNGE, in order to remain functional.
> 
> If it's implementing COPY/EXPUNGE anyway, there is merit in *not*
> implementing MOVE, as there are then fewer codepaths - whether this
> outweighs any advantage in implementing anyway depends on deployment
> levels and perceived gain to the client developer.
> 
> So, like most things, this is a trade-off, and I'm not convinced it's
> worthwhile given the small gains for client developers.
Tb, which is one of the most popular IMAP clients (at least for desktops) 
already includes this functionality (as XAOL-MOVE). So client authors are 
willing to trade.
 
> > > b) MOVE may be simpler for server authors. However, server authors
> > > who cannot implement COPY are themselves at a disadvantage.
> > 
> > MOVE is a basic command, and the fact that it wasn't included in
> > original
> > specification with arguments that 'it cannot be atomic' it's just
> > dumb. The
> > fact that specification author can't make it atomic in his own
> > server code is
> > not an argument to me
> 
> You're getting very close to calling Mark dumb. I don't agree, but
> even if this were the case, this wouldn't invalidate Mark's arguments
> - which are *not* that his own implementation makes it difficult to
> implement, but that the majority of extent implementations make it
> difficult to implement with "IMAP-like" semantics.
I'm far from calling Mark dumb. I just think that lack of MOVE, at least as an 
extension (if it cannot be provided in some cases) is a failure of the 
protocol design.
I already know few IMAP servers implementators that are ready to implement it 
(or have already started implementing it), and this draft is online for just 4 
days. 


> > About removal: read the draft (version 01) carefully. The possible
> > outcomes of
> > MOVE command are:
> > 1. MOVE is succesful, server sends untagged EXPUNGE responses and
> > then tagged
> > OK,  mails are removed from a mailbox but they're still there - in
> > another
> > mailbox. Client knows everything about internal state of currently
> > selected
> > mailbox.
> > 
> > 2. (case of 'emulation' by copy+expunge, or more internally by
> > link+delete)
> > MOVE command is unsuccesful, messages are duplicated in both
> > mailboxes, no
> > EXPUNGE responses are sent, client knows everything about internal
> > state of
> > currently selected mailbox (nothing has changed)
> > 
> > 3. MOVE command is totally unsuccesful, no messages are moved, no
> > EXPUNGE
> > responses are sent, client knows...
> 
> Yes, I know you're attempting to cover all the issues, here, I just
> happen to be of the opinion that there are a lot of corner cases, and
> many of these you seem to be blissfully unaware of - for instance:
What other cases can there be that are not included here (and are not corner-
cases that are not covered by IMAP specs and exists in eg. EXPUNGE or COPY 
commands)? I really though on that and (not including cases in which COPY 
could also fail) can't think of anything else.

> I note that in the case of success, you refer to messages in the
> UIDPLUS response code after they've already been expunged. A
> conformant client might struggle with that a bit, since it's already
> dropped the messages locally.
>
> Perhaps this needs to say that COPYUID is sent as an untagged OK
> prior to any EXPUNGE being sent referring to any message in the set.
> 
> I'd imagine that for some implementations, this would lead to a
> sequence of COPYUID/EXPUNGE pairs, which is unfortunate, when it
> could have been a single COPYUID/VANISHED.
Caching EXPUNGE responses waiting for final OK seems to be the answer.

> Also, the specification needs to ensure that it clearly marks the
> MOVE command as one that causes ambiguity as described in ?5.5 of
> RFC 3501 - in fact, the ambiguity is quite complex, here, because
> there are multiple failure cases.
> 
> Finally, your case 2 and 3 are perceived as identical by a client
> according to your draft. I'd suggest that an OK is made equivalent to
> a COPY's OK, and that the removal of source messages is indicated
> solely by the untagged EXPUNGEs - thought this would also be covered
> by my first comment.
And that's constructive criticism that I've been waiting for, thank you.

> And these are just the issues I've spotted now. I don't know how I
> can make this clearer - this is a very complex extension to design
> such that it'll work safely in all implementations.
It doesn't have to work in all implementations, and making it work is a 
implementator, not specification designer, work. 

> > > d) If MOVE turns out to be hard in an existing, conformant
> > > implementation, then the implementation is somehow broken.
> > 
> > No, it means that this implementation wasn't written with MOVE in
> > mind from
> > the beginning. Hence it's an -optional- -feature-, not an
> > obligation. And it's
> > a feature that is already being implemented.
> 
> Right, fine, but your argument was that it's perfectly acceptable for
> a server to have broken or slow COPY.
No, my argument was that COPY can be slower than MOVE on some implementations. 
I've never stated that COPY can be broken. 

> a) The extension is, in principle, a good idea.
> 
> b) However, it's the kind of fundamental semantic that needs to be
> defined such that it is implementable in all existing implementations.
What semantic is this missing that is not missing for COPY & EXPUNGE commands 
possible failures?
 
> c) Ideally, this would result in a MOVE command (or facility of some
> form) which would become part of the de-facto core.
That's in ideal world...

-- 
Witold Kr?cicki

Grupa o2 Sp??ka z o.o., ul. Jutrzenki 177, 02-231 Warszawa,
KRS 0000140518, S?d Rejonowy dla m.st. Warszawy, 
Kapita? zak?adowy 377.298,00 z?., NIP 521-31-11-513

