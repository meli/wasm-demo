MBOX-Line: From mrc at CAC.Washington.EDU  Wed Sep  7 09:01:31 2005
To: imap-protocol@u.washington.edu
From: Mark Crispin <mrc@CAC.Washington.EDU>
Date: Fri Jun  8 12:34:36 2018
Subject: [Imap-protocol] SELECT of same mailbox
In-Reply-To: <5b93232d050907075812659c02@mail.google.com>
References: <UsPqTMxta5Q38J/4ek2JBA.md5@bluegrass.trish.de>
	<5b93232d050907075812659c02@mail.google.com>
Message-ID: <Pine.OSX.4.63.0509070829570.28350@pangtzu.panda.com>

On Wed, 7 Sep 2005, Rob Siemborski wrote:
> The second paragraph you quote could probably be more clearly stated
> with "SELECT or EXAMINE of any valid mailbox" or similar, which is how
> I find myself reading "another" in this case.

That was the intended meaning.  I admit that the case of selecting the 
same mailbox was never considered.  My server also always starts a new 
session even on an extraneous SELECT.

However, I do not think that we should require either behavior.  I'm also 
worried about declaring as "broken" servers which appear to comply now. 
What's more, I'm thinking that the ambiguity is a feature, not a bug.

Suppose a popular client appears that does repetitive unnecessary SELECTs 
(maybe because its programmer doesn't understand selected state and thinks 
that this is the only way to acquire mailbox metadata), and in defense the 
server management decides to no-op extraneous SELECT other than pooting 
out the metadata normally announced at SELECT time.  This would have the 
effect of breaking the behavior that extraneous SELECT clears the \Recent 
flags; but if the ambiguity persists then a client can not count upon that 
behavior.

That point, I think, is important: clients should NOT use extraneous 
SELECT to clear \Recent flags.  If a client really wants that, it could 
LOGOUT and start a new session (or use UNSELECT if that extension is 
available).  But it really should use different internal algorithms that 
do not impact the server this way.

My preference would be either to:
  . leave the current wording as-is
or
  . change it to an explicit statement that server behavior (and subsequent
    state of \Recent flags) with extraneous SELECT is undefined and
    implementation dependent.
That way, existing servers are not declared broken, and clients are put on 
notice not to use this as a hack to clear \Recent flags.

-- Mark --

http://panda.com/mrc
Democracy is two wolves and a sheep deciding what to eat for lunch.
Liberty is a well-armed sheep contesting the vote.

