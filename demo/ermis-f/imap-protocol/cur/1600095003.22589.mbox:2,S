MBOX-Line: From blong at google.com  Fri May 22 17:17:52 2015
To: imap-protocol@u.washington.edu
From: Brandon Long <blong@google.com>
Date: Fri Jun  8 12:34:55 2018
Subject: [Imap-protocol] COPYUID message order
In-Reply-To: <555FBFD2.1030800@ymail.com>
References: <555FBFD2.1030800@ymail.com>
Message-ID: <CABa8R6tpd-2GPoSTX0_d8wnROvQpm4_zmMxconJDPPud3NNdww@mail.gmail.com>

At least for Gmail, we always re-order the given set into sequence order
(which is the same as uid order) before doing any operation.  So, the
source UIDs in COPYUID are in the order in which they were copied.

Since a COPY in Gmail is the equivalent of adding a label to a message, the
message may already have that label, so it may already be in the recipient
mailbox.  We always return the destination UIDs in the order which matches
the source UID order, so in your example, source UID 1 is destination UID
2, and source UID 2 is destination UID 1.

When APPEND'ing a duplicate message, we update the UID assigned to the
original to be the newest UID in the mailbox (and that's returned with
APPENDUID), since we found clients typically assumed that was the case and
could get confused.

We could have done this with COPY, I guess, but we didn't see any client
issues in testing, so we left it this way since the performance is better.

Brandon

On Fri, May 22, 2015 at 4:46 PM, Bill Shannon <bill.shannon@ymail.com>
wrote:

> I'm unclear on the requirements around the COPY command and the COPYUID
> response code.
>
> Is there any guarantee that the order of messages in the COPY command is
> the order they'll appear in the destination mailbox?  Or are they
> allowed to appear in any arbitrary order in the destination mailbox?
> (After any existing messages, of course.)
>
> RFC 4315 says of the COPYUID response:
>
>       The source UID set is in the order the message(s) were copied; the
>       destination UID set corresponds to the source UID set and is in
>       the same order.
>
> Exactly what order is that?  Is it the order in which the messages were
> mentioned in the COPY command?  Is it the order they actually appear in
> the destination mailbox (assuming it can be different)?  Or is it in
> some other undefined order?
>
> With Gmail I'm getting
>
> A1 COPY 1:2 dest
> A1 OK [COPYUID 123456 1:2 2,1]
>
> and
>
> A1 COPY 2,1 dest
> A1 OK [COPYUID 123456 1:2 2,1]
>
> The order of the source UIDs is unrelated to the order the messages
> are mentioned in the COPY command and unrelated to the order they
> appear in the destination mailbox.
>
> If I don't know the UIDs of the source messages, is there any way to
> determine which message was copied to which UID in the destination
> mailbox?
> _______________________________________________
> Imap-protocol mailing list
> Imap-protocol@u.washington.edu
> http://mailman13.u.washington.edu/mailman/listinfo/imap-protocol
>
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mailman13.u.washington.edu/pipermail/imap-protocol/attachments/20150522/3292e886/attachment.html>
