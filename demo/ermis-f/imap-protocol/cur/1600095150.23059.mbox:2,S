MBOX-Line: From MRC at CAC.Washington.EDU  Wed Sep  7 12:50:36 2005
To: imap-protocol@u.washington.edu
From: Mark Crispin <MRC@CAC.Washington.EDU>
Date: Fri Jun  8 12:34:36 2018
Subject: [Imap-protocol] SELECT of same mailbox
In-Reply-To: <DC4E58A90D144DDA3A10024D@ninevah.cyrusoft.com>
References: <8613FB1A1827570EC6B5150F@ninevah.cyrusoft.com>
	<Pine.OSX.4.63.0509071108110.28350@pangtzu.panda.com>
	<DC4E58A90D144DDA3A10024D@ninevah.cyrusoft.com>
Message-ID: <Pine.WNT.4.64.0509071235060.3496@Shimo-Tomobiki.panda.com>

On Wed, 7 Sep 2005, Cyrus Daboo wrote:
> a SELECT INBOX foobar

That is not a SELECT command, any more than
 	{ select } choose your option
or
 	to select the best candidate for the job
are.

> I consider the above to be a SELECT command with invalid arguments.

Do my previous examples demonstrate why this is a fallacy?  The notion of 
"a SELECT command with invalid arguments" implies that it is possible to 
define such a thing as distinct from random nonsense; and furthermore that 
it is desirable to do so.

Either it is a valid IMAP command, or it is nonsense.  It muddies the 
waters otherwise.

> So in my 
> interpretation the command is recognised as a 'SELECT command', but the 
> arguments are wrong and that causes the BAD. So when we have the statement:
>       Consequently, if a mailbox is selected and a SELECT command that
>       fails is attempted, no mailbox is selected.
> I consider 'fails' to mean either 'NO' or 'BAD'.

If the response is BAD, there is nothing to fail.  Nothing happened. 
That's the whole point of BAD.  Otherwise, we could make NO be returned 
for all non-success responses, much as POP3 uses "-".

> I guess all I am asking for is a change to that sentence, something like:
>       Consequently, if a mailbox is selected and an attempted SELECT command
>       fails with a NO response, no mailbox is selected.

I think that this is a terrible idea.  It implies that there are other 
cases where there is "failure" involving a BAD.  Thus, every single point 
where command failure is mentioned would have to add such wording, now and 
forever.

I would not object to adding a more specific statement of the general 
principle of BAD.

Nor would I object to adding an example that refers to and restates this 
principle, e.g.
     Consequently, if a mailbox is selected and a SELECT command that
     fails is attempted, no mailbox is selected.

 	Note: remember that a BAD response is not a failure; it indicates
 	that the command itself was not recognized.  Therefore, a string
 	that happens to have SELECT as the second space-delimited token
 	is not a SELECT command unless it complies with the syntax of the
 	SELECT command; and if it does not comply with the syntax of the
 	SELECT command it does not cause the unselection mentioned above.
 	This applies not just to SELECT but to all commands, thus the fact
 	that a note like this doesn't appear with other commands should
 	not be construed to state that this does not apply to those other
 	comands.

However, I think that doing this is silly.

-- Mark --

http://staff.washington.edu/mrc
Science does not emerge from voting, party politics, or public debate.
Si vis pacem, para bellum.

