MBOX-Line: From rfs9999 at earthlink.net  Fri Jan 30 14:52:10 2015
To: imap-protocol@u.washington.edu
From: Rick Sanders <rfs9999@earthlink.net>
Date: Fri Jun  8 12:34:53 2018
Subject: [Imap-protocol] Zimbra and FETCH response
Message-ID: <54CC0B1A.5040701@earthlink.net>

Hi,

This may be due to my incomplete understanding of the IMAP RFC. My 
apologies if that's the case.

The most recent release of Zimbra has changed to the way it closes its 
response to FETCH.  Previously when FETCHING various items the response 
for each message would be terminated by ')' on a new line.  That's what 
I have used for years and it works for all other servers I have worked 
with (Gmail, Dovecot, CommuniGate, Openwave, etc).  But not Zimbra now.

 >> 1 FETCH 1:* (uid flags internaldate RFC822.SIZE 
body.peek[header.fields (From Date Message-Id Subject)])

For example here is what Gmail sends:

<< * 1 FETCH (UID 23910 RFC822.SIZE 17599 INTERNALDATE "16-Jan-2015 
19:28:27 +0000" FLAGS (NonJunk) BODY[HEADER.FIELDS (From Date Message-Id 
Subject)] {211}
<< From: comp.mail.pine@googlegroups.com
<< Subject: Digest for comp.mail.pine@googlegroups.com - 2 updates
<< Message-ID: <e89a8f92403a1c5886050cc9fa6d@google.com>
<< Date: Fri, 16 Jan 2015 19:28:27 +0000
<<
<< )
<< * 2 FETCH (UID 23911 RFC822.SIZE 14881 INTERNALDATE "16-Jan-2015 
20:30:20 +0000" FLAGS (NonJunk) BODY[HEADER.FIELDS (From Date Message-Id 
Subject)] {210}
<< From: comp.mail.imap@googlegroups.com
<< Subject: Digest for comp.mail.imap@googlegroups.com - 1 update
<< Message-ID: <485b397dd00762fb3c050ccad70b@google.com>
<< Date: Fri, 16 Jan 2015 20:30:20 +0000
<<
<< )

In contrast Zimbra sends:

<< * 1 FETCH (UID 10191 INTERNALDATE "03-Dec-2014 10:10:53 -0200" 
RFC822.SIZE 9219 BODY[HEADER.FIELDS (FROM DATE MESSAGE-ID SUBJECT)] {238}
<< From: joe <joe@abc.com>>
<< Subject: test message
<< Date: Wed, 3 Dec 2014 10:10:07 -0200
<< Message-ID: <201412031009340.006629001417608574@pmta05>
01-30-2015.19:38:32 <<
01-30-2015.19:38:32 <<  FLAGS (\Recent))
01-30-2015.19:38:32 << 1 OK FETCH completed

The last line of the response is FLAGS not ')'.  I've had to add special 
logic when Zimbra is involved to consider FLAGS as the end.

One of my customers recently upgraded to Zimbra and immediately the 
application had used for years stopped working.  :-)  And other Zimbra 
users have reported the problem to me as well.

What am I missing or doing wrong?

Thanks,
Rick

-- 
Rick Sanders
rfs9999@earthlink.net
IMAP Tools  http://www.athensfbc.com/imap-tools

