MBOX-Line: From mrc+imap at panda.com  Tue Aug 23 09:26:12 2011
To: imap-protocol@u.washington.edu
From: Mark Crispin <mrc+imap@panda.com>
Date: Fri Jun  8 12:34:46 2018
Subject: [Imap-protocol] IMAP 4 extensions for Addressbook
In-Reply-To: <B812C7DC-C83D-440D-A32A-5C65C30AA71E@iki.fi>
References: <4E5353C1.1020008@geograph.co.za>
	<20110823112015.176047jadao2tej3@webmail.uni-hohenheim.de>
	<B812C7DC-C83D-440D-A32A-5C65C30AA71E@iki.fi>
Message-ID: <alpine.OSX.2.00.1108230842070.1200@hsinghsing.panda.com>

On Tue, 23 Aug 2011, Timo Sirainen wrote:
> I guess CardDAV would be the preferred solution.

Indeed.

Email, contacts management, calendars, tasks, and configuration are all
difficult problems.  They are even more difficult when it is required to
synchronize these across multiple agents and consumers.

Each of these have very different environment and requirement sets.  You
can not assume that techniques suitable for one are equally good (or even
usable) for another.  This was a hard-earned lesson of the 1990s: file
sharing is not the way to distribute email.

The inevitable result of shoehorning all of these into a single
agent/protocol are bloated dinosaurs that don't do anything particularly
well.  After all the years I spent hearing people rant about the wonders
of Groupwise and Exchange, I have now spent three years dealing with the
true horrors of both.

Very bright people work on CalDAV, CardDAV, etc.  It is slow going because
these problems are very hard.  It is the height of arrogance, not to
mention folly, to believe "oh, just make an extension to IMAP and the
problem is solved."

Now, with that said, some agents have implemented baby forms of remote
address book within the framework of IMAP and have done so without
extension.

For example, Pine and Alpine have long supported remote address book and
configuration by writing the data as messages to a mailbox (e.g.,
ADDRBOOK) on the IMAP server.  This is very simple-minded; the entire
address book is a single message in ASCII; and thus address book updates
are simply appending a new message (and, presently, deleting/expunging the
old message).

This isn't as bad as it sounds: my complete contacts list in VCF form is
only 68K.  My Alpine address book, which only contains what Alpine needs,
is more like 3K.

This isn't fancy-schmany.  There's no synchronization or off-line access.
When Alpine starts, it merely provisions itself from the remote address
book.  It'll notice when the address book changes, but all changes are a
complete replacement.

And, of course, global address book needs are done separately by LDAP.

Configuration is done similarly.

This really is a baby implementation, but it works well enough to deal
with the immediate pressure.  I've even heard that other MUAs are capable
of reading Alpine's address books, at least to the point of importing the
data into its own address book.

Most importantly, every IMAP server in the world supports it, NOW, because
it requires no extension or change to IMAP.

-- Mark --

http://panda.com/mrc
Democracy is two wolves and a sheep deciding what to eat for lunch.
Liberty is a well-armed sheep contesting the vote.

