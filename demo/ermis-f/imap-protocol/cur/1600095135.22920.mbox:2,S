MBOX-Line: From mrc at CAC.Washington.EDU  Sun Mar 11 22:25:09 2007
To: imap-protocol@u.washington.edu
From: Mark Crispin <mrc@CAC.Washington.EDU>
Date: Fri Jun  8 12:34:38 2018
Subject: [Imap-protocol] body size in BODYSTRUCTURE: include headers?
In-Reply-To: <07Mar11.210244pst."57996"@synergy1.parc.xerox.com>
References: <07Mar11.210244pst."57996"@synergy1.parc.xerox.com>
Message-ID: <alpine.OSX.0.83.0703112208440.20984@pangtzu.panda.com>

On Sun, 11 Mar 2007, Bill Janssen wrote:
> It's not clear (to me) from RFC 3501 whether the following "body size"
> value in BODYSTRUCTURE include's the part's headers, or not.  I'm assuming
> it does.

You are mistaken, and I am completely bewildered as to how you could make 
that assumption.

After all, you quoted text from the specification that says that both the 
body size and size in lines are "the size in its transfer encoding and not 
the resulting size after any decoding".  The MIME headers are neither 
encoded or decoded, that that statement would be ludicrous if headers were 
included.

It would also be extraordinarily silly for the protocol to include the 
MIME headers in these sizes.  MIME headers are not included in the BODY 
content which you fetch it, so why would you want a number that is the 
added size of two items (the MIME headers and the BODY content)?  And if 
it did that, why wouldn't it include the delimiter?  Or whoknowswhatelse?

Now, the reason why the size of the body in octets is so important is that 
when you do a partial fetch of, say, BODY[2] (which does not include the 
MIME headers) you know how many bytes you have to fetch (since it's a 
partial fetch and not a full-fetch where you could just swallow whatever 
the server shoves at you).  If the MIME headers were included in the size, 
then you would first have to fetch the MIME headers, calculate that string 
length, and then subtract that value from the body size in order to get 
the number that you actually need.

Maybe this will help you.  Consider how a message is represented in the 
native IMAP constructs of ENVELOPE, BODYSTRUCTURE, and BODY[n] (ignore 
those HEADER, TEXT, and MIME addons).  Now, consider what makes sense in 
that context.  It seems that IMAP is a protocol that goes to considerable 
effort to free a client from having to deal with skanky things like 
headers.  Perhaps IMAP might even represent messages in other formats 
(this doesn't actually happen, but it *was* part of the original 
architecture).

Once you understand that, you will have a pretty good razor to help you 
resolve these kinds of questions.

-- Mark --

http://panda.com/mrc
Democracy is two wolves and a sheep deciding what to eat for lunch.
Liberty is a well-armed sheep contesting the vote.

