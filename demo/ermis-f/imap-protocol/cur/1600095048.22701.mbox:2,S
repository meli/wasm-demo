MBOX-Line: From dwhite at olp.net  Sat Nov 17 23:44:35 2012
To: imap-protocol@u.washington.edu
From: Dan White <dwhite@olp.net>
Date: Fri Jun  8 12:34:49 2018
Subject: [Imap-protocol] Re: Cannot authenticate plain with Cyrus server
In-Reply-To: <50A81C5B.3010109@comaxis.com>
References: <50A81C5B.3010109@comaxis.com>
Message-ID: <20121118074434.GA8424@dan.olp.net>

On 11/17/12?15:23?-0800, Jeff McKay wrote:
>I have client software trying to do AUTHENTICATE PLAIN against a Cyrus
>imap server version 2.2.12.  I have a SSL connection. I am getting
>authentication failure even though I'm pretty sure I am doing this
>correctly. The original auth response I generate is this:
>
>64 6F 67 67 79 00 63 6F 6D 70 75 67 65 6E 00 63 doggy.compugen.c
>6F 6D 70 75 67 65 6E 00 00 00 00 00 00 00 00 00 ompugen.........
>
>where "doggy" is the user id and "compugen" is both the administrator
>user and password.  I base64 encode this string to:
>
>ZG9nZ3kAY29tcHVnZW4AY29tcHVnZW4=
>
>and send that in response to the server's '+' prompt, but get back "NO
>authentication failure".  I can do a normal login using compugen/compugen
>and it works, and that gives me access to all user folders.  I am also
>certain that "doggy" is a valid user id, because I have logged in with
>that as well.
>
>I have used this exact same software with many other servers to do
>authenticate plain with no problem.  Since I can do a normal login with
>compugen, that suggests a work around I can use, but I would prefer to
>know what is wrong with auth plain, or know for certain that there is a
>problem on the server that is not my fault.

By requesting separate authc and authz identities, you are asking Cyrus
IMAP to perform (what it calls) proxy authentication. Your 'admin' user
will need to either:

be listed within the 'proxyservers' config option in /etc/imapd.conf
   or
have 'a' acl rights on authz's mailbox (and have loginuseacl enabled)

See the imapd.conf manpage for your version as my experience is based on a
newer version of cyrus.

I believe that having a user listed within both 'admins' and 'proxyservers'
will fail proxy authentication for that user (by design).

Also have a look at your syslog (auth facility for cyrus sasl output, and
mail facility for cyrus imap output). Configuring mail.* and auth.* may
provide more detail as to why your authentication attempts are failing.

-- 
Dan White

