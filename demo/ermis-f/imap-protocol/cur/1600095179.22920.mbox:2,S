MBOX-Line: From MRC at CAC.Washington.EDU  Fri Mar 16 16:29:37 2007
To: imap-protocol@u.washington.edu
From: Mark Crispin <MRC@CAC.Washington.EDU>
Date: Fri Jun  8 12:34:38 2018
Subject: [Imap-protocol] what IMAP extensions to implement?
In-Reply-To: <07Mar16.123633pst."57996"@synergy1.parc.xerox.com>
References: <07Mar16.123633pst."57996"@synergy1.parc.xerox.com>
Message-ID: <alpine.WNT.0.83.0703161613040.1748@Tomobiki-Cho.CAC.Washington.EDU>

On Fri, 16 Mar 2007, Bill Janssen wrote:
> I'm looking for recommendations as to what extensions make sense to
> implement in my server.  Generally speaking, this is a mix of adding
> true functionality and reasonably wide-spread client support.  I've
> already added IDLE and NAMESPACE; what else makes sense?

Going through the current set of standards-track and approved 
(SORT and THREAD are both blocked on i18n) extensions:

must-have:	LOGINDISABLED, STARTTLS
  ;; requirement of the specification

should-have:	MULTIAPPEND, SORT, THREAD, UIDPLUS
  ;; some clients are heavily impacted if absent

ought-to-have:	BINARY, IDLE, UNSELECT
  ;; some clients are impacted if absent

nice-to-have:	CATENATE, CHILDREN, CONDSTORE, ESEARCH, LITERAL+,
 		 NAMESPACE, SASL-IR, URLAUTH
  ;; some clients will benefit if present; note that CATENATE and
  ;; URLAUTH are part of the mandatory "trio" for Lemonade

if meaningful:	ACL, LOGIN-REFERRALS, MAILBOX-REFERRALS, QUOTA
  ;; may not be meaningful on your server

your choice:	ID
  ;; diagnostic purposes

Some people may disagree with me on some of these boundaries; consider 
them to be fuzzy.  I am aware of some clients which would consider some of 
the "nice-to-have" extensions important enough to kick up into a higher 
category.

The bottom line is that very few extensions stand out as being worthless 
with the possible exception of the two referral extensions, which seem to 
have fallen by the wayside.

-- Mark --

http://staff.washington.edu/mrc
Science does not emerge from voting, party politics, or public debate.
Si vis pacem, para bellum.

