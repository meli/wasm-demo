MBOX-Line: From Corby.Wilson at nokia.com  Tue Sep 13 06:19:29 2005
To: imap-protocol@u.washington.edu
From: "Corby.Wilson@nokia.com" <Corby.Wilson@nokia.com>
Date: Fri Jun  8 12:34:36 2018
Subject: [Imap-protocol] SELECT of same mailbox
Message-ID: <D9ECB8614A9A1340BC8944F8C8B311690124C1BD@daebe102.NOE.Nokia.com>

This is confusing the heck out of me.
There should not be this level of ambiguity anywhere in any RFC.

The way I interpreted the statements:

OK - Command Successfully Executed
BAD - Command failed and the state of any part of the server is exactly
the same as it was before the command was submitted (except for maybe
the Z flag).  This can be a syntax error, a hardware failure on the
server side, or executing two commands at the same time that collide
with each other (no proper atomicity).
NO - The command was executed but something prevented successful
completion.  State may have changed meaning the client should do
appropriate cleanup if necessary.

As Cyrus pointed out, the AUTHENTICATE command works like this.

I had assumed this was the case, but now I see that the meaning of these
changes throughout the document and I will need to revisit each command.
In the future I will make sure I'm very clear and explicit on meaning
and that there is no room for interpritation.

Corby Wilson

 
 

> -----Original Message-----
> From: imap-protocol-bounces@mailman1.u.washington.edu 
> [mailto:imap-protocol-bounces@mailman1.u.washington.edu] On 
> Behalf Of ext Cyrus Daboo
> Sent: Thursday, September 08, 2005 12:47 PM
> To: Mark Crispin
> Cc: imap-protocol@u.washington.edu
> Subject: Re: [Imap-protocol] SELECT of same mailbox
> 
> Hi Mark,
> 
> --On September 7, 2005 12:50:36 PM -0700 Mark Crispin 
> <MRC@CAC.Washington.EDU> wrote:
> 
> >> a SELECT INBOX foobar
> >
> > That is not a SELECT command, any more than
> >  	{ select } choose your option
> > or
> >  	to select the best candidate for the job
> > are.
> 
> In many Sections in rfc3501:
> 
> >                BAD - command unknown or arguments invalid
> 
> To me this implies a difference between an unknown command 
> 'verb' and a 
> known command 'verb' but with invalid arguments. Yet you say 
> that a command 
> with invalid arguments is an unknown command or at least not 
> the same as 
> the command that uses the 'verb'.
> 
> rfc3501, Section 2.2.2:
> 
> > BAD (indicating a protocol error such as
> >    unrecognized command or command syntax error)
> 
> Again a distinction is being made between an unrecognised 
> command and a 
> command with syntax errors, contrary to your statements.
> 
> So, I still believe that:
> 
> a SELECT INBOX foobar
> 
> is a SELECT command with a syntax error. I wonder how many 
> others also see 
> it that way?
> 
> 
> >> I guess all I am asking for is a change to that sentence, 
> something like:
> >>       Consequently, if a mailbox is selected and an 
> attempted SELECT
> >>       command fails with a NO response, no mailbox is selected.
> >
> > I think that this is a terrible idea.  It implies that 
> there are other
> > cases where there is "failure" involving a BAD.  Thus, 
> every single point
> > where command failure is mentioned would have to add such 
> wording, now
> > and forever.
> 
> The problem is you are using the work 'failure' to mean a 
> 'NO' response is 
> returned. My dictionary defines failure as 'lack of success' 
> - i.e. any 
> response other than 'OK' as far as IMAP is concerned. Form a client 
> implementer perspective both 'NO' and 'BAD' represent errors 
> (and that to 
> me means failure). The term 'failure' is used in 3501 in many several 
> different contexts. So lets look at 3501:
> 
> Section 2.2.2:
> 
> >    The server completion result response indicates the success or
> >    failure of the operation.
> 
> To me that means 'BAD' is considered a 'failure' since its a 
> completion 
> response and clearly does not indicate 'success'. Later on in 
> that same 
> paragraph it says 'OK' is 'success', 'NO' is 'failure' and 'BAD' is 
> 'error'. So there is a lack of consistency at least there.
> 
> Section 7.1.5:
> 
> >       The difference between a BYE that occurs as part of a normal
> >       LOGOUT sequence (the first case) and a BYE that 
> occurs because of
> >       a failure (the other three cases) is that the 
> connection closes
> >       immediately in the failure case.
> 
> This is clearly talking about a 'generic' failure as opposed 
> to a 'NO' 
> failure.
> 
> Section 6:
> 
> >    The state of a connection is only changed by successful commands
> >    which are documented as changing state.  A rejected command (BAD
> >    response) never changes the state of the connection or of the
> >    selected mailbox.  A failed command (NO response) 
> generally does not
> >    change the state of the connection or of the selected 
> mailbox; the
> >    exception being the SELECT and EXAMINE commands.
> 
> This is a very clear statement of the SELECT problem, but in 
> terms of a 
> 'generic' state change.
> 
> Section 6.2.2:
> 
> >       If an AUTHENTICATE command fails with a NO response, 
> the client
> >       MAY try another authentication mechanism by issuing another
> >       AUTHENTICATE command.
> 
> Here the specific case of 'NO' vs 'BAD' is described to avoid 
> confusion. 
> That is really all I am asking for for the SELECT issue. Perhaps a 
> reference back to that paragraph at the top of Section 6 is 
> really all that 
> is needed.
> 
> Alternatively, if you are going to redefine the terms 
> 'failure' and 'fails' 
> to mean a 'NO' response (as opposed to their usual dictionary 
> meaning) then 
> they should be explicitly described in the 'Conventions' section and 
> perhaps even be quoted whenever that explicit meaning is used 
> as opposed to 
> the regular dictionary meaning.
> 
> -- 
> Cyrus Daboo
> _______________________________________________
> Imap-protocol mailing list
> Imap-protocol@u.washington.edu
> https://mailman1.u.washington.edu/mailman/listinfo/imap-protocol
> 

