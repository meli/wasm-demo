MBOX-Line: From janssen at parc.com  Mon Mar 12 13:19:45 2007
To: imap-protocol@u.washington.edu
From: Bill Janssen <janssen@parc.com>
Date: Fri Jun  8 12:34:38 2018
Subject: [Imap-protocol] how long can APPEND take?
Message-ID: <07Mar12.121950pst."57996"@synergy1.parc.xerox.com>

Due to my current back-end architecture, adding a mail message to a
folder can take a considerable amount of time, up to 10 minutes or so,
depending on whether attachment content has to be OCR'ed for the
Lucene index.  It looks to me as if the semantics of APPEND, this may
be unacceptable:

      If the mailbox is currently selected, the normal new message
      actions SHOULD occur.  Specifically, the server SHOULD notify the
      client immediately via an untagged EXISTS response.

Nothing's going to happen "immediately".  Of course, I presume that
"immediately" in this context means, "whenever the server first
notices the new message".  I can hang the connection for N minutes or
so till the result of the attempted message incorporation is known;
the client can always open another connection in the meantime.  Then
I can respond once the APPEND is complete.

Comments?

Bill

