MBOX-Line: From mrc+imap at panda.com  Mon Nov 14 12:37:42 2011
To: imap-protocol@u.washington.edu
From: Mark Crispin <mrc+imap@panda.com>
Date: Fri Jun  8 12:34:47 2018
Subject: [Imap-protocol] [noob] multiple fetch responses for the same
	message?
In-Reply-To: <29330.1321289315.045850@puncture>
References: <43724F01-C3E3-4039-8919-51E528C56C54@mac.com>
	<1321215266.30428.140660998383617@webmail.messagingengine.com>
	<755EBF71-00B6-4A4A-940A-CA8B5B62B10D@mac.com>
	<4EC02C84.1090209@gulbrandsen.priv.no>
	<alpine.OSX.2.00.1111131306210.38075@hsinghsing.panda.com>
	<29330.1321262033.662442@puncture>
	<alpine.OSX.2.00.1111140822340.38075@hsinghsing.panda.com>
	<29330.1321289315.045850@puncture>
Message-ID: <alpine.OSX.2.00.1111141220490.38075@hsinghsing.panda.com>

On Mon, 14 Nov 2011, Dave Cridland wrote:
>> Such clients deserve to be tripped up.
> I don't disagree... But I'm also pragmatic enough to consider that
> insufficient motivation to avoid tripping them up if practical.

If you were the Very Big Corporation Of Monopoly, you would be happy to
trip up those clients in order to drive them out of business.

You already trip up other vendors' products with your violations of the
standard. The problem is that gets you flamed and may attract unwelcome
attention by regulators. It's better, if you can arrange it, to trip up
other vendors' products with your unexpected compliance with the standard,

> Well, it's just plain wrong if it's been requested, and it's weird if
> it hasn't been.

Yes. On the other hand, a client that implemented IMAP according to the
standard would have no problem either way.

> Again, I don't disagree - even if internally in Polymer, the UID *is*
> the primary key.

Polymer is not the only implementation with that internal implementation.
However, that leads to two deadly traps for incautious implementors (and
I know that you are not one of these and certainly do it right in
Polymer):

The first trap is to confuse your internal implementation with IMAP. Time
and time again, I have seen internal implementations that were SIMILAR to
IMAP UIDs, but did not have the same precise semantics. Not good.

The second trap is failing to implement message sequence numbers as a
primary key in the client-facing part. This may be as simple as a vector,
indexed by message sequence number, to the internal key. You may, if and
ONLY if there is an exact sematic mapping between IMAP UID and internal
key, shortcut the UID->MSN->internal mapping.

Too often, I see the shortcut when the semantics aren't the same, and I
see truly bizarre (and dysfunctional!) implementations of MSN.

I won't tell you about the implementation that, for each command,
enumerated all the UIDs and then did a bubble sort in order to implement
MSNs (and UID ranges). Oh I guess I just did. Sorry if that made you puke
your lunch.

-- Mark --

http://panda.com/mrc
Democracy is two wolves and a sheep deciding what to eat for lunch.
Liberty is a well-armed sheep contesting the vote.

