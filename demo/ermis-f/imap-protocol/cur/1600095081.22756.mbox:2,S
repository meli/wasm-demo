MBOX-Line: From mrc+imap at panda.com  Mon Nov  7 10:31:50 2011
To: imap-protocol@u.washington.edu
From: Mark Crispin <mrc+imap@panda.com>
Date: Fri Jun  8 12:34:47 2018
Subject: [Imap-protocol] [noob] select & unseen?
In-Reply-To: <201111071808.pA7I8EtY029928@mxout11.cac.washington.edu>
References: <C61A1BDF-03DD-4ED9-BFCA-C6183F07DD3E@mac.com>
	<4EB72A0C.31264.813832F8@David.Harris.pmail.gen.nz>
	<alpine.OSX.2.00.1111061650360.9034@hsinghsing.panda.com>
	<1320653922.13384.140660995615489@webmail.messagingengine.com>
	<alpine.OSX.2.00.1111070726260.9034@hsinghsing.panda.com>
	<201111071808.pA7I8EtY029928@mxout11.cac.washington.edu>
Message-ID: <alpine.OSX.2.00.1111071013340.9034@hsinghsing.panda.com>

On Mon, 7 Nov 2011, Pete Maclean wrote:
> there is no way to indicate in a SELECT response that there are no
> unseen messages.

That is unfortunately correct.

> However, if I understood one of
> Mark's comments correctly, it seems likely that many clients will not
> care about this information in isolation but only message flags in general.

Also correct.

> I did suggest to this customer that an unsolicited STATUS response
> could be sent of the form:
> * STATUS Mailbox (Unseen 0)

That would be a good idea; but there is a problem.

First, it will break any IMAP2 (RFC 1176) client. That is much less of a
concern today than it was 15 years ago. Yet we frequently find ourselves
having to deal with issues from much older software and even more obsolete
specifications.

Second, it presumes that the client knows what such an unsolicited STATUS
response means at that point.

Third, no matter how many times I say "clients MUST be prepared to accept
ANY response at ANY time (and, at worst, ignore a response that doesn't
seem to make sense in this context), there are clients that will puke when
they receive unsolicited responses that they think are "impossible" at
that point in the session.

That's probably fatal for the idea. It would otherwise be a nice and
elegant solution.

It was, in fact, proposed back then. The proposal died for those reasons.
Nobody was happy about that since it was a nice and elegant solution.

The only way we could have done it is by a new select-class command
(EXTSELECT?). In retrospect, that is what we should have done. Instead, we
kept a single IMAP2-compatible SELECT command. IMAP2 compatibility is also
why we hijacked untagged OK to carry response codes. It was very important
at the time.

Anyone got a time machine?

-- Mark --

http://panda.com/mrc
Democracy is two wolves and a sheep deciding what to eat for lunch.
Liberty is a well-armed sheep contesting the vote.

