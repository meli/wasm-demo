MBOX-Line: From slusarz at curecanti.org  Mon Nov 26 14:57:21 2012
To: imap-protocol@u.washington.edu
From: Michael M Slusarz <slusarz@curecanti.org>
Date: Fri Jun  8 12:34:49 2018
Subject: [Imap-protocol] Re: Suspend/Restore feature proposal
In-Reply-To: <CABa8R6tGF8Uj-MEec6vdHrhkYefhiU0QLGtVXafDwOXb3vnFmQ@mail.gmail.com>
References: <20121115215854.Horde.zz6B0O0tmt3ylHiEXzXhQQ4@bigworm.curecanti.org>
	<CABa8R6tHP2My0k2LqT1RzHoLQZA+X_jwUMU0cydm2sAwo8f4Fg@mail.gmail.com>
	<20121116143137.Horde.7Kb3aEW5DhM6CnuA2hAx4Q8@bigworm.curecanti.org>
	<e122fa25-9110-4b36-855d-0e7e273c5805@flaska.net>
	<20121121155417.Horde.ZeW7JqTPNxTAI-hTtrAT-Q9@bigworm.curecanti.org>
	<CABa8R6sJfnA=HYk-fmFfpXki91cne_LeqFyuhDebMGeDdsdj7g@mail.gmail.com>
	<20121121183606.Horde.8DDdRoJ261AbQsAiMPfS4w1@bigworm.curecanti.org>
	<CABa8R6u62m0SkAoLa+K8MgQn8CXKxtv5Hnnt6O1muTZQnFqqoA@mail.gmail.com>
	<20121126152005.Horde.UGsoLB91esWQF9_aGi6UYQ1@bigworm.curecanti.org>
	<CABa8R6tGF8Uj-MEec6vdHrhkYefhiU0QLGtVXafDwOXb3vnFmQ@mail.gmail.com>
Message-ID: <20121126155721.Horde.a3j2EWFDFOo-xkaflrURBQ2@bigworm.curecanti.org>

Quoting Brandon Long <blong@google.com>:

> By polling, I assume you mean for calling STATUS?

Well hopefully LIST-STATUS exists on the server, because that makes a  
huge performance difference. (For some reason, a larger number of  
users will whine and complain incessantly unless the ability to poll  
all mailboxes is enabled. Why anybody wants to poll mailboxes other  
than those in which messages are being delivered are beyond me. But  
this is a feature that users demand, so we have to support it  
unfortunately.)

> Imagine, instead of a "special case" resume, that we're instead treating
> this as a client that often re-connects.  You'd treat IMAP the same way a
> normal "connected" client would, and the server would just keep your
> connection in the same connection state, possibly holding any results it
> would send you.  You would just "reconnect/resume" and then issue the next
> command.  You'd be immediately back in the selected state, but there
> wouldn't be any cost associated with it (from the client side) except
> perhaps having to update client state if the server gives you updates, but
> that just results in faster updates of server state.

This would be great.  But implementation of this would be orders of  
magnitude more difficult than the more simple SUSPEND case and, at  
least in part, would be duplicating behavior of QRESYNC.

> You could even mimic Outlook by having a separate "virtual connection" that
> handles STATUS calls, and one "virtual connection" for actual folder
> actions.

This doesn't help for disconnected clients though.

> This is essentially trying to turn IMAP into a more HTTP like protocol.

Not necessarily a bad thing. It would be fantastic if there was an  
option to send "quick" commands that look like:

AUTHENTICATE user password FETCH <imap url>

But such a drastic change is no longer IMAP 4.  So not really worth  
discussing on this list.

> It is more expensive for a server to offer this, though as long as the
> client has to "request" a session, it would actually be cheaper for the
> server to maintain that state than re-loading it between connections.

What I get out of this is that you think QRESYNC was the result of an  
incorrect design decision and, instead,  the proposal you previously  
linked to in this thread should have won out.

michael


