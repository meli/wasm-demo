MBOX-Line: From MRC at Washington.EDU  Thu Mar  6 17:51:07 2008
To: imap-protocol@u.washington.edu
From: Mark Crispin <MRC@Washington.EDU>
Date: Fri Jun  8 12:34:41 2018
Subject: [Imap-protocol] LSUB with the %
In-Reply-To: <47D099C5.9040805@stalker.com>
References: <47D099C5.9040805@stalker.com>
Message-ID: <alpine.WNT.1.00.0803061734020.1280@Tomobiki-Cho.CAC.Washignton.EDU>

On Fri, 7 Mar 2008, Roman Prokhorov wrote:
> E.g. if I'm subscribed to "User/somedropbox", why would I need to discover 
> that I'm also subscribed to "User" if I'm not? Assuming that 'LSUB *' will 
> return only User/somedropbox - any client can get confused.

Consider the situation in which User/somedropbox is subscribed, but User 
is not.  The client issues
 	tag LSUB "" %

The server can not return "User/somedropbox" because that does not match 
the pattern.  "User" isn't subscribed.  So how does the hierarchical 
browsing client find out that there's a "User/somedropbox" that is 
subscribed?

The answer is that it returns "User" to LSUB with the \NoSelect flag, to 
indicate that "User" is not subscribed, but it has at least one descendent 
(child, grandchild, great-grandchild, whatever) which is subscribed.

> If some client uses a sequence of 'LSUB %', then 'LSUB found/%', etc for 
> reading the tree of subscriptions - this is a problem of the client, and it 
> should not be transferred to all servers and clients.

You just described the way that ALL well-behaved clients prowl the 
hierarchy with LIST.  Only poorly-written clients use a * wildcard with 
LIST.

It is not unreasonable that a hierarchical browsing client would do the 
same for LSUB.

> If I have lookup right on 'one/two' mailbox but no lookup right on 'one' - 
> does it mean that the same "special situation" should occur for LIST command 
> which on 'LIST "~someuser" "%"' should show me 'one' (despite of the fact 
> that I don't have lookup right for it) if a 'one/two' mailbox exist which for 
> I do have lookup right?

This is something that an implementation would have to decide for itself. 
However, if LIST won't show "one" but will show "one/two", then there is 
no way for a hierarchical browser can discover "one/two".  Your users may 
complain.

-- Mark --

http://staff.washington.edu/mrc
Science does not emerge from voting, party politics, or public debate.
Si vis pacem, para bellum.

