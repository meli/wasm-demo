MBOX-Line: From guenther+imap at sendmail.com  Thu Jun  7 16:02:55 2007
To: imap-protocol@u.washington.edu
From: Philip Guenther <guenther+imap@sendmail.com>
Date: Fri Jun  8 12:34:39 2018
Subject: [Imap-protocol] Concurrent Mailbox Changes.
In-Reply-To: <alpine.OSX.0.99.0706070855180.11475@pangtzu.panda.com>
References: <46581509.7050201@buni.org> <1180182602.32181.1919.camel@hurina>
	<alpine.OSX.0.99.0705260804430.3501@pangtzu.panda.com>
	<1181230565.6639.15.camel@mike-laptop>
	<alpine.OSX.0.99.0706070855180.11475@pangtzu.panda.com>
Message-ID: <Pine.BSO.4.64.0706071648360.5267@vanye.mho.net>

On Thu, 7 Jun 2007, Mark Crispin wrote:
...
> Here is what that other client will see:
> 	a001 FETCH ...
> 	* n FETCH ...
> 	a001 OK FETCH completed
> 	a002 FETCH ...
> 	* n FETCH ...
> 	* 8 EXISTS
> 	* 3 RECENT
> 	a002 OK FETCH completed
> 	a003 NOOP
> 	* 2 EXPUNGE
> 	* 3 EXPUNGE
> 	* 6 EXISTS
> 	* 3 RECENT
> 	a003 OK NOOP completed

Note that the final EXISTS response is not required here: the previous 
EXISTS said eight and there have been exactly two EXPUNGE responses, so 
the client already knows that exactly six messages remain.  RFC 3501, 
section 7.4.1, p2:
       The EXPUNGE response also decrements the number of messages in the
       mailbox; it is not necessary to send an EXISTS response with the
       new value.

The RECENT response clearly needs to be sent whenever the number messages 
with the \recent flag has changed.  It's probably technically legal to 
suppress it when that count hasn't changed (say, if new messages have 
arrived that aren't \recent or if an equal number of \recent messages have 
been expunged as have arrived), but it may not be wise for a server to do 
that.


Philip GUenther

