MBOX-Line: From ingo.schurr at schlund.de  Wed Nov 30 09:08:21 2005
To: imap-protocol@u.washington.edu
From: Ingo Schurr <ingo.schurr@schlund.de>
Date: Fri Jun  8 12:34:36 2018
Subject: [Imap-protocol] INBOX magic
Message-ID: <20051130170821.GC13650@fone>

Hi Folks,

I guess this is asked a thousand times, sorry for asking it time 1001:

From RFC 3501
mailbox         = "INBOX" / astring
                    ; INBOX is case-insensitive.  All case variants of
                    ; INBOX (e.g., "iNbOx") MUST be interpreted as INBOX
                    ; not as an astring.  An astring which consists of
                    ; the case-insensitive sequence "I" "N" "B" "O" "X"
                    ; is considered to be INBOX and not an astring.
                    ;  Refer to section 5.1 for further
                    ; semantic details of mailbox names.
                    
Reading the BNF as pedantic as possible, neither DQUOTE "Inbox" DQUOTE
nor "Inbox/foo" would trigger the INBOX magic, as both would be
astrings. (In the example '/' would be a hierarchical separator)

It seems to me the intended reading would be: the root folder "INBOX"
is case-insensitive. That is, (with '/' as separator again)
DQUOTE "InBox" DQUOTE   -> INBOX
"{5}" CR LF "inbox" -> INBOX
"InBox/foo"             -> INBOX/foo
"InBoxOffice"           -> InBoxOffice
(on an otherwise case-sensitive server).

This reading for sure would avoid potential strange behaviour. If not:
a001 CREATE InBox/foo
a002 LIST "" "*"
* LIST () "/" INBOX
* LIST () "/" InBox
* LIST () "/" InBox/foo
a003 DELETE InBox
-> Ups, all mails in "INBOX" are gone...

Best,

Ingo




