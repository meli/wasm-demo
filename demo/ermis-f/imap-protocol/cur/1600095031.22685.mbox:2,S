MBOX-Line: From tss at iki.fi  Fri Feb  8 08:55:14 2013
To: imap-protocol@u.washington.edu
From: Timo Sirainen <tss@iki.fi>
Date: Fri Jun  8 12:34:50 2018
Subject: [Imap-protocol] Clarification on RFC 6851
In-Reply-To: <51129513.5080905@aol.com>
References: <51129513.5080905@aol.com>
Message-ID: <1360342514.3230.53.camel@hurina>

On Wed, 2013-02-06 at 12:38 -0500, Stuart Brandt wrote:
> I'm hoping someone can clarify the following section of the new MOVE 
> extension:
> 
>     Because a MOVE applies to a set of messages, it might fail partway
>     through the set.  Regardless of whether the command is successful in
>     moving the entire set, each individual message SHOULD either be moved
>     or unaffected.  The server MUST leave each message in a state where
>     it is in at least one of the source or target mailboxes (no message
>     can be lost or orphaned).  The server SHOULD NOT leave any message in
>     both mailboxes (it would be bad for a partial failure to result in a
>     bunch of duplicate messages).  This is true even if the server
>     returns a tagged NO response to the command.
> 
> 
> In the case where a MOVE fails partway through the set and leaves some 
> messages moved and others unaffected, does this imply that servers 
> SHOULD return a NO in the tagged response, or is OK the more appropriate 
> tagged response?

I was waiting for someone else to give a more exact answer, maybe
referring to the RFC text, but here's my take on it. I think the only
two reasonable server implementations are:

a) Return OK if you moved all of the messages, NO if you didn't move
anything. (This is similar to how COPY works.)

b) Return OK if you moved all messages that currently exist, i.e. you
skipped over messages recently expunged by another session. Return NO if
you didn't move anything. (I'm not a fan of this implementation.)

I don't think a server should leave the mailbox in a state where some of
the messages were copied and some of them weren't, except if something
exceptionally bad happens (e.g. server crash). If server happens to do
that anyway in normal code, it should return NO.



