MBOX-Line: From slusarz at curecanti.org  Mon Dec 31 12:31:18 2012
To: imap-protocol@u.washington.edu
From: Michael M Slusarz <slusarz@curecanti.org>
Date: Fri Jun  8 12:34:50 2018
Subject: [Imap-protocol] Missing UIDNEXT during mailbox synchronization
In-Reply-To: <1356733058.19335.140661171123477.7DDFF57D@webmail.messagingengine.com>
References: <db123879-1082-4911-b7ce-3a861d62217a@flaska.net>
	<20121228112725.Horde.cM7EjLQoqQDix9dQMs5KcA1@bigworm.curecanti.org>
	<1356723190.15576.140661171078629.69B791C0@webmail.messagingengine.com>
	<20121228130129.Horde.VTgbGtjlIvg0wUwlkp5dDg6@bigworm.curecanti.org>
	<1356732787.18647.140661171122177.5EB821AB@webmail.messagingengine.com>
	<1356733058.19335.140661171123477.7DDFF57D@webmail.messagingengine.com>
Message-ID: <20121231133118.Horde.U5PPDhJmrIJk-ldhlJw3KQ1@bigworm.curecanti.org>

Quoting Bron Gondwana <brong@fastmail.fm>:

>> > > The more interesting question is: what do you need to know it  
>> for anyway?
>> >
>> > To determine if the mailbox has changed to prevent an issuance of an
>> > unneeded FETCH/SORT command.  Flag changes are irrelevant (if
>> > CONDSTORE isn't available), since clients can be designed to not care
>> > about these changes
>>
>> Fair enough I guess, if you really don't need the data from those  
>> intermediate
>> records.  Otherwise you may as well just do fetch $olduid+1:*  
>> (stuff you need),
>> which will be the same number of round trips as your solution in the trivial
>> case, and one fewer in the case where there is new data to know.

If UIDNEXT doesn't change, why would you be doing a fetch?

>> Let's play make believe for a second with Cyrus though:
>>
>> . select inbox
>> * 2 EXISTS
>> * 2 RECENT
>> * FLAGS (\Answered \Flagged \Draft \Deleted \Seen)
>> * OK [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen \*)] Ok
>> * OK [UNSEEN 1] Ok
>> * OK [UIDVALIDITY 1356732706] Ok
>> * OK [UIDNEXT 3] Ok
>> * OK [HIGHESTMODSEQ 5] Ok
>> * OK [URLMECH INTERNAL] Ok
>> * OK [ANNOTATIONS 65536] Ok
>> . OK [READ-WRITE] Completed
>> . fetch 1:* (uid)
>> * 1 FETCH (UID 1)
>> * 2 FETCH (UID 2)
>> . OK Completed (0.000 sec)
>> . fetch * (uid)
>> * 2 FETCH (UID 2)
>> . OK Completed (0.000 sec
>
> Sorry - cat on keyboard failure.

I have a dog, so I tend to not have these kind of issues :).

> )
> . store 2 +flags \Deleted
> * 2 FETCH (FLAGS (\Recent \Deleted))
> . OK Completed
> . expunge
> * 2 EXPUNGE
> . OK [HIGHESTMODSEQ 7] Completed
> . fetch * (uid)
> * 1 FETCH (UID 1)
> . OK Completed (0.000 sec)
>
> That definitely doesn't work!

Why not?  The stored UIDNEXT != the cached UIDNEXT.  So something is  
up and the client should issue FETCH's to retrieve further  
information.  UIDNEXT's have nothing to do with the actual next  
assigned UID, so I don't care if it is incrementing or decrementing.   
It only matters if it is different.

> . uid fetch * (uid)
> . OK Completed (0.000 sec)

I may have mis-typed before.  A UID FETCH * is not useful - it should  
be a FETCH * (UID) used instead to determine the last UID in the  
mailbox.  And if the mailbox is empty, I agree that the actual UIDNEXT  
value can't be determined.  In our client, we clear the UIDNEXT and  
cache so the next time a message is seen the cache is restart/rebuilt.

michael


