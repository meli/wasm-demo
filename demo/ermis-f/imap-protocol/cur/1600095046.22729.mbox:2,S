MBOX-Line: From tss at iki.fi  Thu May  3 10:18:42 2012
To: imap-protocol@u.washington.edu
From: Timo Sirainen <tss@iki.fi>
Date: Fri Jun  8 12:34:48 2018
Subject: [Imap-protocol] Concurrent flag changes
In-Reply-To: <19F04AF2-EFBB-4A85-9333-73DE0A6FF476@isode.com>
References: <14DB8F84-E3D5-426A-BBF6-5B4746B6E3DF@iki.fi>
	<19F04AF2-EFBB-4A85-9333-73DE0A6FF476@isode.com>
Message-ID: <B502BC26-D0CE-4879-B355-D16339F3D034@iki.fi>

On 3.5.2012, at 13.47, Alexey Melnikov wrote:

> Hi Timo,
> 
> On 2 May 2012, at 22:28, Timo Sirainen <tss@iki.fi> wrote:
> 
>> I don't think IMAP RFCs actually require any specific behavior for this, so this is more of a "any recommendations?" type of a question:
>> 
>> Clients C1 and C2 send:
>> 
>> C1: a FETCH 1 FLAGS
>> S1: * 1 FETCH (FLAGS (\Seen \Answered))
>> S1: a OK
>> 
>> C2: b STORE 1 +FLAGS (\Flagged)
>> S2: * 1 FETCH (FLAGS (\Seen \Flagged))
>> S2: b OK
>> 
>> At this point C1 still thinks that 1's flags are (\Seen), and being a little bit stupid it unsets the \Seen flag by sending:
>> 
>> C1: c STORE 1 FLAGS (\Answered)
>> 
>> Now, I think the possible replies are either of these:
>> 
>> S1: * 1 FETCH (FLAGS (\Answered))
>> S1: * 1 FETCH (FLAGS (\Answered \Flagged))
>> 
>> Dovecot currently sends the first reply, but I've started thinking that perhaps I should change it to the second one. The question is really: Should STORE FLAGS be thought of as
>> 
>> a) Reset all the flags that you have currently, whatever they are, and only set these flags.
> 
> Yes.
> 
> If you want to prevent conflicts above, you need to use CONDSTORE.

Yes, but that's a client issue. And I think with STORE (UNCHANGEDSINCE) FLAGS it's clear that the client wants to do this b) :

>> b) Atomically add these flags I've listed, and remove those that I used to see previously in this session but aren't listed here.

while without CONDSTORE it's a bit more ambiguous what the client wants to do.

>> In the case of actual IMAP clients doing this, this is probably almost irrelevant. But it becomes more relevant if you have two IMAP servers doing a 2-way mailbox synchronization after a possibly long disconnection, and the same message's flags are changed in both of them.
> 
> Don't use STORE FLAGS when synchronizing servers ;). I don't.

Not servers, but after client has issued such updates how would the servers sync them. For example I want this to at least work:

Server 1 has seen +FLAGS \Seen
Server 2 has seen +FLAGS \Flagged

When syncing the servers the result clearly should be (\Seen \Flagged). Similarly for -FLAGS. But FLAGS itself is less clear how to merge them.
