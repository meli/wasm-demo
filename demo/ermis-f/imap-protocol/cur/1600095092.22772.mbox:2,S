MBOX-Line: From slusarz at curecanti.org  Thu May 19 23:26:07 2011
To: imap-protocol@u.washington.edu
From: Michael M Slusarz <slusarz@curecanti.org>
Date: Fri Jun  8 12:34:46 2018
Subject: [Imap-protocol] Thoughts on keyword improvements/enhancements
In-Reply-To: <alpine.OSX.2.00.1105191513210.24932@hsinghsing.panda.com>
References: <20110518011645.Horde.6sTkboF5lbhN03Jd3XNndCA@bigworm.curecanti.org>
	<BANLkTikVbKdXg01nnajJ-=XLNfTpjjjG0Q@mail.gmail.com>
	<alpine.OSX.2.00.1105181108180.24932@hsinghsing.panda.com>
	<20110518210716.GA12636@brong.net>
	<alpine.OSX.2.00.1105181519330.24932@hsinghsing.panda.com>
	<20110519045851.GA23466@brong.net>
	<alpine.OSX.2.00.1105182203320.24932@hsinghsing.panda.com>
	<20110519220352.GA12141@brong.net>
	<alpine.OSX.2.00.1105191513210.24932@hsinghsing.panda.com>
Message-ID: <20110520002607.Horde.lf1bSoF5lbhN1gl-aoNHDLA@bigworm.curecanti.org>

Hmmm... didn't realize my seemingly innocuous email requesting  
comments would cause such a vigorous, wide-ranging discussion on a  
variety of subjects.  However, in no large part due to my own selfish  
interests, this is my attempt to summarize/expand on the feedback  
generally related to the original post.

Here's what (I think) we learned:

* Option A: Feasibility of extending keywords entirely within RFC 3501  
framework

Timo/Bron both present the idea that it would be technically possible  
to use the existing keyword limitations (atoms; case insensitive) to  
store non-ASCII data.  A solution involving using a special character  
to define a "keyword+", such as #, along with encoding UTF7/UTF8 data  
in a case-insensitive manner, e.g. base32, is suggested (I would  
actually suggest good old base16.  Since base32 requires MANDATORY '='  
padding - see RFC 4648 [6] - base16 in many situations will actually  
be shorter.  Plus, hex data is already prevalent in various contexts  
so developers would most likely have a greater comfort level in  
working with this kind of data).

The downside of this approach is that legacy clients would have  
unfettered access to these new keywords.  Although many clients ignore  
keywords they are unfamiliar with, other clients don't.  We can  
probably all agree that these clients presenting keywords such as  
'#666F' (base16 encoding of 'foo') to clients may not be the best of  
ideas.  We would be preserving backward compatibility at the expense  
of providing a label that means absolutely nothing to a user.

However, this limitation could be worked around with use of ENABLE.   
Thus, these newer keywords/labels would not be visible to legacy  
servers.

* Option B: Creating a new class of keywords called "Labels"

Brandon suggested that the Gmail method of X-GM-LABELS  
(http://code.google.com/apis/gmail/imap/#x-gm-labels) be used.
X-GM-LABELS are essentially keywords except they take ASTRINGS,  
encoded w/UTF7-IMAP, instead of atoms.  Although Google treats labels  
interchangably as folders, the concept could be used as an extension  
of keywords also.

This solution would require definition of a store modifier to the  
STORE/UID STORE commands (RFC 3501[6.4.6] - or maybe a store modifier  
under RFC 4466 [2.5]), along with new SEARCH criteria (RFC 3501  
[6.4.4]) and FETCH CRITERIA (RFC 3501 [6.4.5]).  The APPEND command  
would most likely need to be altered (or a new command defined) to  
allow for specifying labels when saving a message. (This list of  
potential spec alterations is not meant to be exhaustive.)

* Option C: Use keywords, provide keyword <-> label lookup

Another option, that did not receive much (if any) interest, would  
require NO additional extension of the IMAP protocol.  This method  
would be akin to the way that many clients work today: use regular RFC  
3501 keywords to mark a message, and then internally do a translation  
to the label when displaying to the end user.

This translation table could be made globally accessible to all mail  
clients through the METADATA extension.  A server-wide entry (RFC 5464  
[3.2.1.1, 6.2]) could potentially be defined and used as a  
standardized way to do keyword -> label lookups across various MUAs  
accessing the mail store.  This solution would require registration of  
an IANA entry, but would not require a substantive change to the IMAP  
spec.


Not sure if I am missing any other option, or another viable combo of  
the concepts listed above.

To add a bit more to the discussion, an issue from the client side  
that is important to address:

It would be useful for a client to have a list of all labels available  
on the server that can be applied to the messages.  These labels could  
then be used in a UI element to allow for easy labeling of messages,  
using labels that have already been previously defined by the user.

At a minimum, this would need to be available on a per-mailbox basis.   
This issue seems easy enough: a new response code [PERMANENTLABELS]  
(?) could be output on a SELECT/EXAMINE which lists all labels used in  
the mailbox.  This response code would be ignored by legacy clients  
pursuant to RFC 3501 [7.1].  The alternative, requiring the client to  
grab all flags/keywords in a mailbox and parse for the  
flags/keywords/labels used, is untenable for large mailboxes.

However, as a practical matter, labels should be globally available in  
all mailboxes.  For example, a TODO label would be useful in any  
mailbox.  Additionally, this would potentially facilitate client UI,  
as a client could, for example, provide a single unified page of  
labels to allow the user to edit rather than requiring editing in each  
mailbox (or requiring the client to loop through every mailbox to  
globally change a label).

Finally, and this might be overkill, but possibly implement some sort  
of MODSEQ-like counter to track label changes.  When labels change,  
the MODSEQ-like number is incremented.  This would allow a client to  
skip synchronizing the label list on every login if not needed.   
However, given that the number of labels in general might be limited  
(most people use no more than 5-10?), the cost might outweigh the  
benefit.

michael


