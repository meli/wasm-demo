MBOX-Line: From jkt at flaska.net  Tue Jun 12 07:49:57 2012
To: imap-protocol@u.washington.edu
From: =?UTF-8?B?SmFuIEt1bmRyw6F0?= <jkt@flaska.net>
Date: Fri Jun  8 12:34:48 2018
Subject: [Imap-protocol] QRESYNC and new arrivals which get deleted
	immediately through VANISHED
In-Reply-To: <E96EC1B2-A6D2-4BF6-9004-8248F61015F5@isode.com>
References: <4FD221F9.8090601@flaska.net> <4FD4EB4B.7050000@flaska.net>
	<E96EC1B2-A6D2-4BF6-9004-8248F61015F5@isode.com>
Message-ID: <4FD75715.2040607@flaska.net>

Hi Alexey,

On 06/11/12 21:08, Alexey Melnikov wrote:
>> Shall I remove any messages upon receiving the VANISHED response?
>> Shall I send a UID SEARCH command to find out what new messages are
>> really there? That'd complicate my code quite a lot,
>> unfortunately.
> 
> In most cases you don't need to do anything. For example if you only
> cached a part of the mailbox (e.g. N most recent messages), and UIDs
> listed in VANISHED are below any UID you know about, then you don't
> really care about removed messages.
> 
> It might make a difference if you want to display the number of
> messages in the mailbox or use message numbers for some operations.
> If you cached the whole mailbox, then you know the whole msgno-to-UID
> map. If you only cached a part, then maybe doing something like "UID
> FETCH *" (or a similar SEARCH) would help.

I always establish full seq -> UID mapping as the very first thing when
I open a mailbox. The API through which I make the data about a mailbox
accessible to the rest of the application really wants to know how many
messages are there in a mailbox at any time; showing a total number of
messages in a mailbox is one of the use cases, but not the only one.
That's why I react to an EXISTS either by:

- doing nothing if the number is the same as my current idea of the
number of messages,
- throwing an error if the server tried to decrease the number of
messages against what I believe shall be there,
- asking for UIDs of the new arrivals immediately through UID FETCH
(FLAGS) immediately.

As soon as my idea of the "total number of messages" begins to differ
from the server's idea, I have trouble. That's an assumption on which
I've built my client; it allows me to deal with EXPUNGEs properly, etc.
My client will essentially show just a "loading..." placeholder until
the UID for a message arrives.

However, now QRESYNC seems to threaten this model because the server
reports increases in the amount of messages through EXISTS and their
removals through VANISHED. The issue is that there's a window where
VANISHED can refer to messages whose UID is unknown to the client and
that the protocol explicitly allows VANISHED to refer to UIDs which have
never existed in the mailbox -- at least according to my understanding.
I'd love to be proved wrong.

Unless I'm mistaken, the only completely safe way out of that is to
issue UID SEARCH ALL (or the ESEARCH equivalent) when a VANISHED
referencing "unknown" UIDs arrives at the time the mailbox has messages
whose UID I don't know yet.

Would you think that this will fly?

Would a proposal for adding "* ARRIVED 64,65,66" to replace EXISTS have
any real-world chance of getting accepted?

>> Even more entertaining would be an interaction where the server 
>> occasionally sent a regular EXPUNGED instead of VANISHED (it's
>> allowed to do so per RFC 5162); that'd lead to an interesting mix
>> of unknowns about a mailbox' state...
> 
> I hope no server does that (unless in different sessions, when
> QRESYNC is enabled in one and not another...).

Agreed. It's just that now I've implemented the RFC in my client, I'm
afraid there are ways of getting it out-of-sync through "legal"
responses; I'd like to mitigate that.

With kind regards,
Jan

-- 
Trojita, a fast e-mail client -- http://trojita.flaska.net/

-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 270 bytes
Desc: OpenPGP digital signature
URL: <http://mailman13.u.washington.edu/pipermail/imap-protocol/attachments/20120612/7a5fc86d/attachment.sig>
