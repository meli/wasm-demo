MBOX-Line: From Pidgeot18 at verizon.net  Thu Jul 10 16:59:23 2008
To: imap-protocol@u.washington.edu
From: Joshua Cranmer <Pidgeot18@verizon.net>
Date: Fri Jun  8 12:34:42 2018
Subject: [Imap-protocol] Questions re RFC 3501
Message-ID: <4876A25B.8010503@verizon.net>

To start off, a bit of background information:

I am currently working on writing a comprehensive IMAP testing server 
for use in automated regression tests for the email clients Thunderbird 
and SeaMonkey. If you would like to see more, you can view my trials and 
tribulations at <https://bugzilla.mozilla.org/show_bug.cgi?id=437193>.

I'll admit that I have not gotten around to reading all of the IMAP RFCs 
yet, nor have I read the entire archives yet, so I apologize in advance 
if I ask a question answered in one of those two forums.

These are some questions that pop into my mind when reading the RFC:
1. Which parts are case-sensitive and which parts are not? The only 
explicit mentions I see are that literals in the ABNF are 
case-insensitive, and that base64 strings are case sensitive (for 
obvious reasons), that INBOX is case-insensitive, and that other mailbox 
names are explicitly not defined with respect to case-sensitivity.

2. Can hierarchy delimiters change recursively (e.g., "Users" has a 
delimiter of "/" but "Users/news" has a delimiter of ".")? To what 
degree do multiple hierarchy delimiters exist in nature?

3. Is there any definitive list of which servers support which 
extensions, including how they don't support them correctly (okay, that 
wasn't about the RFC itself)?

4. Are there any explicit pitfalls I might want to test or look out for 
when writing the testing server?

Thanks in advance for your response.

-- 
Joshua Cranmer

Beware of bugs in the above code; I have only proved it correct, not tried it. -- Donald E. Knuth


