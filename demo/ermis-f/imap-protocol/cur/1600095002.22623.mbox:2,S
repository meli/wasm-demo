MBOX-Line: From asuth at mozilla.com  Tue Sep  2 13:19:45 2014
To: imap-protocol@u.washington.edu
From: Andrew Sutherland <asuth@mozilla.com>
Date: Fri Jun  8 12:34:53 2018
Subject: [Imap-protocol] Seeking clarity on Gmail "Access for less
 secure apps" setting for non XOAuth2 access
In-Reply-To: <6f3e9961-32e6-4b4b-866f-7ce5526b0bf8@gulbrandsen.priv.no>
References: <5400A146.4020602@mozilla.com>	<CABa8R6se2WefF4q-cFzR2qtU_5_jDL-wioPF+jPmOTdpCaJhtw@mail.gmail.com>	<54011E24.4080209@mozilla.com>
	<6f3e9961-32e6-4b4b-866f-7ce5526b0bf8@gulbrandsen.priv.no>
Message-ID: <54062661.6020000@mozilla.com>

On 08/30/2014 05:02 PM, Arnt Gulbrandsen wrote:
>> and want to better handle:
>> - Please log in via your web browser: 
>> http://support.google.com/mail/accounts/bin/answer.py?answer=78754 
>> (Failure)
>
> There is a response code for that, WEBALERT. Not standard, but at 
> least it's better than trying to have a client parsing human-readable 
> text.

It appears Gmail may actually be doing this, which is great news! In a 
tangential bug of ours, a reporter using openssl s_client -connect gave 
us the following sanitized excerpt:

* NO [WEBALERT ***] Web login required.
a001 NO [ALERT] Please log in via your web browser: 
http://support.google.com/mail/accounts/bin/answer.py?answer=78754 (Failure)

A similar full example with URL seems to be provided at 
https://productforums.google.com/forum/#!topic/gmail/BYxYSdThpiw but I'm 
having trouble finding a lot of other examples/details.


It's very likely we missed this since our error handling / logging was 
only reporting the tagged response.  We try and avoid seeking full 
protocol traces/maximum debug logging from users because of privacy 
concerns.  Unfortunately, it does make it harder to learn about 
exceptional situations like this that aren't particularly documented and 
require state that is outside our control to reproduce.


Arnt, thank you very much for putting us on the right track, here! Do 
you know if we should expect WEBALERT to always be untagged, and/or how 
other servers (and what other servers) might use it?  A quick skim of 
open source clients (coremail2, android email) and servers (dovecot, 
cyrus) isn't enlightening me.


I'd like to archive this information in a more publicly available 
fashion for people.  I think http://imapwiki.org/ is probably the best 
resource I know of (other than actual standards :), but I'm open to 
other suggestions!

Thanks!
Andrew

