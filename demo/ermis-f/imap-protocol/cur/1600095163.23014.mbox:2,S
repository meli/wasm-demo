MBOX-Line: From MRC at CAC.Washington.EDU  Tue Apr 11 18:37:39 2006
To: imap-protocol@u.washington.edu
From: Mark Crispin <MRC@CAC.Washington.EDU>
Date: Fri Jun  8 12:34:37 2018
Subject: [Imap-protocol] LIST Clarification
In-Reply-To: <web-35044345@mail.stalker.com>
References: <443A7A2D.2070708@consilient.com> <web-35034698@mail.stalker.com>
	<Pine.OSX.4.64.0604101053530.2906@pangtzu.panda.com>
	<web-35035906@mail.stalker.com>
	<Pine.WNT.4.65.0604101441160.4904@Tomobiki-Cho.CAC.Washington.EDU> 
	<443C0286.60200@att.com>
	<Pine.WNT.4.65.0604111228310.3332@Tomobiki-Cho.CAC.Washington.EDU>
	<syMWpRdWCviH+GmTLGxvWQ.md5@libertango.oryx.com>
	<Pine.WNT.4.65.0604111418130.3332@Tomobiki-Cho.CAC.Washington.EDU>
	<web-35044345@mail.stalker.com>
Message-ID: <Pine.WNT.4.65.0604111814490.3332@Tomobiki-Cho.CAC.Washington.EDU>

On Tue, 11 Apr 2006, Vladimir A. Butenko wrote:
> It would be beneficial if a client can learn AT LEAST if the server namespace 
> is case-sensitive or not. Because any client does have to switch its internal 
> routines to the case-[in]sensitive mode.

Well, the attitude of the past was that the client shouldn't do any such 
thing; that is, that it could not (and should not) make any assumptions 
about how the server works.

I wouldn't be opposed to an extension of this nature, but it may be 
difficult to implement.  A UNIX based server could not assume that it is 
case-sensitive; it would have to determine this on a filesystem basis and 
it may not have a good way of knowing (other than empirical testing) if a 
remote filesystem is case-sensitive or not.

> But, as usual, the Pandora box is happy to be opened again:

You may be interested to know that your Cyrillic example actually 
displayed correctly in my Japanese environment!  :-)

> The underlying OS file system may handle this 
> case properly - Windows does, I'm sure MacOSX does so too. But it won't help -
> as an IMAP server cannot use that OS feature, because it cannot deal directly 
> with non-Latin names: UTF-7 comes in and destroyes everything ;-(

For what it's worth, I just implemented support for Unicode 
case-insensitivity in the development sources of UW imapd.  Actually, I 
cheated and canonicalized everything to titlecase.

Decomposition is next.

> Decoding UTF-7 back into UTF-8 and using it internally is ugly, as you can 
> get a completely different name in LIST results.

Don't the rules for modified UTF-7 define a completely reversible 
transform?

> That's why "strict case-sensitivity" is a GOOD thing from the protocol point 
> of view, and all IMAP servers MUST be case-sensitive to avoid confusion in 
> clients (but not in users). Unfortunately, many IMAP servers do map mailbox 
> names into OS file names, and we have all these "semi-case-insensitivity" 
> problems today.

I don't follow you as to why all IMAP server "MUST be case-sensitive", 
since clearly there are examples of servers which are case-insensitive, 
and I don't see why a server could not decide to map Unicode case for 
M-UTF7 names.

> The solution would be to get rid of UTF-7 and switch to the plain UNENCODED 
> UTF-8 for mailbox names.

I agree.  The whole point of modified UTF-7 in 1996 was to put a halt to 
the (then-common) practice of "just send 8-bits" for mailbox names in 
local character sets.

The people who did (do?) so have had 9 1/2 years warning.  That should be 
enough.  We should progress to UTF-8 mailbox names.

>> Science does not emerge from voting, party politics, or public debate.
> Political Science does :-). There is a lot one can learn by watching primates 
> voting. Or debating. Especially publicly :-)

As I say on my web page, any field of study which has "science" in its 
name is not a science...  ;-)

>> Si vis pacem, para bellum.
> and when you wish for war, prepare for a long boring peace? :-)

I don't know, as only fascists wish for war; and (as the world learned 60+ 
years ago) the only thing that you can do with fascists is defeat them.

-- Mark --

http://staff.washington.edu/mrc
Science does not emerge from voting, party politics, or public debate.
Si vis pacem, para bellum.

