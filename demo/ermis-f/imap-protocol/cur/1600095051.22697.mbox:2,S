MBOX-Line: From alexey.melnikov at isode.com  Thu Dec 13 04:32:57 2012
To: imap-protocol@u.washington.edu
From: Alexey Melnikov <alexey.melnikov@isode.com>
Date: Fri Jun  8 12:34:49 2018
Subject: [Imap-protocol] BINARY for broken MIME parts
In-Reply-To: <CABa8R6tJV4iOjgckqLzEqLcdifRnVj6+g_7cB1vNd3YTgSt4pw@mail.gmail.com>
References: <CDD9BF88-A87D-41CB-A668-E2A7FE5CFE20@iki.fi>
	<50C0A8F4.4070405@isode.com>
	<809DCB83-1507-4F6B-8B70-D86042569C46@iki.fi>
	<CABa8R6tJV4iOjgckqLzEqLcdifRnVj6+g_7cB1vNd3YTgSt4pw@mail.gmail.com>
Message-ID: <50C9CAF9.7040205@isode.com>

On 11/12/2012 06:16, Brandon Long wrote:
>
> On Mon, Dec 10, 2012 at 9:56 PM, Timo Sirainen <tss@iki.fi 
> <mailto:tss@iki.fi>> wrote:
>
>     On 6.12.2012, at 16.17, Alexey Melnikov wrote:
>
>     > On 04/12/2012 11:12, Timo Sirainen wrote:
>     >> Another BINARY issue not specified by the current BINARY RFC:
>     How to handle fetching BINARY[x] or BINARY.SIZE[x] when the MIME
>     part exists but contains invalid base64/quoted-printable data? The
>     possibilities I think would be:
>     >>
>     >> a) Just ignore the invalid data and reply as best as you can.
>     I'm not sure if this is actually useful in any case since the
>     result will be invalid, and it might even cause security problems
>     in case viruses try to exploit it at some point when virus scanner
>     and IMAP server do the decoding differently. I remember reading a
>     few years ago a lot of reports about this issue.
>     >>
>     >> b) Reply NIL to BINARY[x] and 0 to BINARY.SIZE[x] and overall
>     reply OK to the FETCH.
>     >>
>     >> c) Abort and return NO to the FETCH, similarly to as for
>     [UNKNOWNCTE] handling, except without that resp-code.
>     >
>     > I think both b) and c) would be Ok. But b) is probably less
>     likely to surprise client authors.
>     >
>     >> I implemented b) today for Dovecot, thinking that it would
>     probably cause less client confusion than c) and would be more
>     secure then a).
>     >
>     > Maybe it is worth to do b) but also return "UNKNOWNCTE"?
>
>     So:
>
>     a FETCH 1 BINARY[1]
>     * 1 FETCH (BINARY[1] NIL)
>     a OK [UNKNOWN-CTE] Invalid input?
>
>     I don't know how clients currently handle UNKNOWN-CTE. Maybe some
>     would think that if it's returned then the server can never decode
>     that content-transfer-encoding?..
>
>
> I would think [PARSE]

PARSE seems to be Ok for this. It doesn't quite match the definition in 
RFC 3501, but it if fairly close.

> would be more appropriate than UNKNOWN-CTE.

After reviewing its definition, I tend to agree.

CORRUPTION also seems wrong: I think it is typically about mailbox 
storage, i.e. something a mail server administrator might be able to fix.

> Or just stick with [ALERT].

As per other responses, I think this one is wrong.

> Also, what if there is more than one message requested?  We usually 
> just fail to return the broken message and respond NO to the whole 
> fetch, but return all the non-broken messages.
>
> Also, I wouldn't use "Invalid input", maybe its just my thinking, but 
> I would think the "input" is the IMAP command, but its an invalid 
> message data.
>

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mailman13.u.washington.edu/pipermail/imap-protocol/attachments/20121213/b52fdbf3/attachment.html>
