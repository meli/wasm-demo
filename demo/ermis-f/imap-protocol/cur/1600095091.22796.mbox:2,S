MBOX-Line: From imap-protocol at lists.grepular.com  Tue Oct  5 02:04:39 2010
To: imap-protocol@u.washington.edu
From: Mike Cardwell <imap-protocol@lists.grepular.com>
Date: Fri Jun  8 12:34:45 2018
Subject: [Imap-protocol] Fwd: iOS IMAP IDLE (Standard "Push Email")
	Deficiency, Explanation?
In-Reply-To: <11166.1286222622.375011@puncture>
References: <352410DE-19D8-4EE4-8741-AB82741D7A18@sabahattin-gucukoglu.com>
	<4CAA0F5C.7030009@lists.grepular.com>
	<11166.1286222622.375011@puncture>
Message-ID: <4CAAEA27.5060301@lists.grepular.com>

On 04/10/2010 21:03, Dave Cridland wrote:

>> This makes complete sense. In order to use IMAP Idle on a phone, it
>> would have to keep a TCP connection open, and therefore a 3G connection
>> open. There's a reason why phone makers advertise separate stand by, and
>> call times for battery usage. If IMAP idle were being used, the phone
>> would never enter stand by mode, and would eat the battery within a few
>> hours.
>>
> That's somewhat ill-informed, I'm afraid. I did quite a bit of research
> into this for XMPP, and I captrued most of the findings in XEP-0286.
>
> The "3G session" does need to stay open, but it can stay in Idle, or
> PCH, modes. These cost in the region of 8mA.
>
> Small notifications - including the EXISTS and FETCH FLAGS that IDLE
> typically emits from the server - will only raise the 3G session to FACH
> mode. If the handset is forced into FACH mode constantly, this'll drain
> the battery in around 7 hours, using around 140mA.
> 
> Only if the data size reaches a certain (small) threshold - about 128
> octets typically - will the radio rise to DCH mode, where the drain is
> around 380mA - sufficient to drain the battery in three hours.

Ok, I stand corrected. Thanks for the detailed response. I tried to use
IDLE with K-9 on my Android phone a while ago and it seemed to drain the
battery quite quickly so I made some wrong assumptions. Android runs on
a separate CPU to the rest of the phone, and that CPU goes to sleep
unless an application is using it. I guess when in IDLE mode, K-9 needs
to keep a thread running waiting for incoming data, and therefore needs
to keep a wake lock on the CPU. I suspect that might be what is draining
the battery more quickly.

I do some Android development myself, and I can't see how you could
develop an app within the constraints of the Android sandbox which keeps
a tcp connection alive without keeping a wake lock on the cpu and
draining it's battery much more quickly.

Either way, it seems to last a whole lot longer if I just set it to poll
for new mail every 10 minutes and I'm fine with that.

-- 
Mike Cardwell - Perl/Java/Web developer, Linux admin, Email admin
Read my tech Blog -              https://secure.grepular.com/
Follow me on Twitter -           http://twitter.com/mickeyc
Hire me - http://cardwellit.com/ http://uk.linkedin.com/in/mikecardwell

