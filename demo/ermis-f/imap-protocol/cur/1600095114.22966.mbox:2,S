MBOX-Line: From richw at richw.org  Wed Oct 18 16:41:46 2006
To: imap-protocol@u.washington.edu
From: Rich Wales <richw@richw.org>
Date: Fri Jun  8 12:34:38 2018
Subject: [Imap-protocol] Delivery to folder whose name is current year and
	month
Message-ID: <20061018234147.6ED803C36D@whodunit.richw.org>

I run my own Cyrus server.  I'm maintaining a backup archive of
e-mail sent to a particular address, in folders named after the
year and month when a given message was received.

For example (simplifying the real situation just a bit), a message
received right now (October 2006) would be stored in a folder with
a name something like this:    user.richw.Archive.2006-10

A new folder gets created automatically when archived mail shows
up for a new month.  I'm also setting the \Seen flag on the archived
messages as I store them, so they won't be displayed as "unread".

Right now, I'm accomplishing all this via a combination of Procmail
and a custom Perl script that uses the Net::IMAP module.  It works,
but I'd like to consider replacing some or all of my custom stuff
with standard Cyrus tools if possible.  However, I foresee some
serious snags if I were to try this, such as the following:

(1) Sieve doesn't seem to be able to compute a mailbox name on the
    fly (using shell environment variables) -- something I can do
    easily in Procmail.  If I switched to using Sieve to sort my
    incoming mail, I would apparently be restricted to putting
    all archived messages into a single, statically named folder,
    instead of per-month/year folders.

(2) Sieve apparently can't create a given folder if it doesn't
    already exist -- meaning that messages for a new month would
    end up in my default INBOX until I created a folder for the
    new month by hand.  Calling /usr/local/cyrus/bin/deliver from
    Procmail would be even worse -- "deliver" apparently bounces
    mail addressed to a nonexistent folder.  I can create a new
    folder without too much trouble in my custom Perl script
    (using Net::IMAP calls).

(3) Cyrus's "deliver" doesn't allow the setting of any flags, so
    all the archived messages would show up in my IMAP store as
    unread.  It looks like Cyrus used to have a way to set flags
    in its "deliver" command ("-F" flag), but this was taken out
    for some reason.  I realize I can set flags on incoming mail
    in Sieve, but (as I said) it doesn't look like I can compute a
    mailbox name on the fly (for the current month) in Sieve.

Any suggestions?  Would a different IMAP server implementation,
instead of Cyrus, allow me to do what I want?  Or should I just
resign myself to sticking with Procmail and my custom Perl script?

Rich Wales
Palo Alto, CA, USA
richw@richw.org
http://www.richw.org

