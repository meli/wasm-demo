MBOX-Line: From mrc+imap at panda.com  Fri Jun 11 17:21:12 2010
To: imap-protocol@u.washington.edu
From: Mark Crispin <mrc+imap@panda.com>
Date: Fri Jun  8 12:34:44 2018
Subject: [Imap-protocol] IMAP MOVE extension
In-Reply-To: <493C7101-AC65-41BF-B222-E57A150BABB7@iki.fi>
References: <1768814180.483.1276267238967.JavaMail.root@dogfood.zimbra.com>
	<1276267479.22134.102.camel@kurkku.sapo.corppt.com>
	<alpine.OSX.2.00.1006110910440.662@hsinghsing.panda.com>
	<1276274718.22134.138.camel@kurkku.sapo.corppt.com>
	<30B328B7-4D1B-4C7E-96CA-E37F7919E2A6@iki.fi>
	<alpine.OSX.2.00.1006111603200.662@hsinghsing.panda.com>
	<493C7101-AC65-41BF-B222-E57A150BABB7@iki.fi>
Message-ID: <alpine.OSX.2.00.1006111658090.662@hsinghsing.panda.com>

On Sat, 12 Jun 2010, Timo Sirainen wrote:
> 1) Why the messageset parameter? Seems unnecessary to me (along with UID
> PIPELINE command).

Syntactic sugar.  The idea was that the set would be defined by the
pipeline rather than through the first command.  But yes, if you get rid
of that parameter then you don't need a separate UID PIPELINE.

> Also it would be possible to avoid adding | to ATOM-SPECIALS by just
> using more parenthesis:
> a PIPELINE (b COPY 1:100 elsewhere) (c STORE $ +FLAGS \Deleted) (d EXPUNGE)

Not tweaking ATOM-SPECIALS is a good idea.  This is good sugar.

>> Note the $ to repeatedly apply the previous sequence set.
>
> This might conflict with SEARCHRES extension. Or maybe it could just
> "enhance" it.

I forget how SEARCHRES works (and I don't want to look it up), but it
might be nice to do something like

tag PIPELINE (b UID SEARCH ...) (c UID COPY $ elsewhere) (d UID EXPUNGE $)

>> The question is: how many people would actually implement such a facility
>> if it were present?  The normal objections to new capabilities applies as
>> much to this idea.
> A simple IF-OK would be much easier for everyone to initially implement
> than PIPELINE, but maybe PIPELINE wouldn't be too difficult either..

Yup.  The question is whether PIPELINE provides sufficient added value
over IF-OK; and of course whether IF-OK provides sufficient added value to
justify doing it.

The main benefit to PIPELINE over IF-OK is that PIPELINE allows a compiler
on a server to recognize the pipelined operation, and in those few cases
where the server can do an internal move better it could compile it as
that operation.  The significant difference is that, being a pipelined
operation, it isn't required to be atomic; and there is a well-understood
set of error cases.

Unless MOVE is atomic (and a Maildir based one would not be), it creates
completely new error cases that are not covered within the IMAP framework.

I'd probably abstain on both PIPELINE and IF-OK, although I might help
write the specification if it were to happen.  MOVE on the other hand has
too many problems.

-- Mark --

http://panda.com/mrc
Democracy is two wolves and a sheep deciding what to eat for lunch.
Liberty is a well-armed sheep contesting the vote.

