MBOX-Line: From brong at fastmail.fm  Thu May 19 15:03:52 2011
To: imap-protocol@u.washington.edu
From: Bron Gondwana <brong@fastmail.fm>
Date: Fri Jun  8 12:34:46 2018
Subject: [Imap-protocol] Thoughts on keyword improvements/enhancements
In-Reply-To: <alpine.OSX.2.00.1105182203320.24932@hsinghsing.panda.com>
References: <20110518011645.Horde.6sTkboF5lbhN03Jd3XNndCA@bigworm.curecanti.org>
	<BANLkTikVbKdXg01nnajJ-=XLNfTpjjjG0Q@mail.gmail.com>
	<alpine.OSX.2.00.1105181108180.24932@hsinghsing.panda.com>
	<20110518210716.GA12636@brong.net>
	<alpine.OSX.2.00.1105181519330.24932@hsinghsing.panda.com>
	<20110519045851.GA23466@brong.net>
	<alpine.OSX.2.00.1105182203320.24932@hsinghsing.panda.com>
Message-ID: <20110519220352.GA12141@brong.net>

On Wed, May 18, 2011 at 10:45:15PM -0700, Mark Crispin wrote:
> The only time I ever find myself offline is when I am in very remote areas
> of northern British Columbia or the Yukon; and that's only because I chose
> not to cough up the dough for satellite.  Even in rural Alaska, where I
> have to carry a firearm for bear protection on my property, I have
> continuous reliable online access.

Bully for you.  The rest of the world is not necessarily in the same
set of circumstances as you.
 
> >And it's all on an
> >SSD, so searches are stupidly fast even if I wind up doing body scans.
> 
> Except that at some time you had to download the body to that device.
> Given the number of devices that I use, the volume of mail that I deal,
> and the large attachments in that mail, that is not a viable option.

I can do that when I have a fast link.  Again, the world is not you.
It's not a viable option FOR YOU.  Storage for the ~5Gb of email I
currently keep in offlineimap is trivially cheap, and I only had to
sync it once.

> >Besides I'm not sharing my IO queue with 5,000 other connected users when
> >I go to actually access my email.
> 
> If a server can't handle the I/O load for a mere 5000 connected users then
> it has no business being a server for 5000 connected users.

Seriously?  You expect anyone to buy the argument that a server for 5000
concurrent users should be able to give similar random IO performance to
a local dedicated disk for one user?

> >The downside of offlineimap is, as Brendan pointed out for gmail's case,
> >a 100k message mailbox will cause 6Mb of traffic just to synchronise flags,
> >because there's no way to say "tell me what changed since last time".
> 
> That is because of the stupidity of doing such synchronization in the
> first place.  Unless you have such a large screen that it can show the
> flags of 100K messages, there is no reason that the UI needs it.
> 
> A webmail server doesn't transmit data 100,000 messages to the web
> browser.
> 
> Think about that.

Thinking... done.  Gosh, that was quick.  I'm not running a webmail server,
I'm running a local cache that I want to keep in sync with the server so I
can run searches and get identical results.  I want this to work offline.

Did I mention 400ms round trips?  They are noticable compared to local
storage.  That's approximately the distance between Australia and Norway,
and the speed of light isn't that flexible about my wants.

> The only email software that people buy is software that is purposed to
> make email work less well.  There is quite a bit of money in doing that.
> They'll pay quite a bit per-seat for that, but will throw you out of the
> office if you suggest that they pay a fraction as much for an email client
> that does not suck.
> 
> Think about that.

I thought.  Turns out maybe they DO know what they want, and what they want
isn't what you're peddling.  Maybe different features are important to them
than the features that are important to you.

Think about that.
 
> There's no ROI on email clients.  Most people use webmail anyway.  The
> proof is that there are NO good email clients on mobile devices.  If there
> was a market, it would be filled with high-priced competition and people
> would be paying it.  It isn't and they aren't.

True.  So what was your point exactly?

Bron.

