MBOX-Line: From MRC at CAC.Washington.EDU  Tue Feb 28 12:17:50 2006
To: imap-protocol@u.washington.edu
From: Mark Crispin <MRC@CAC.Washington.EDU>
Date: Fri Jun  8 12:34:36 2018
Subject: [Imap-protocol] APPEND flags
In-Reply-To: <44049C49.9010607@sun.com>
References: <44049C49.9010607@sun.com>
Message-ID: <Pine.WNT.4.65.0602281211570.1004@Tomobiki-Cho.CAC.Washington.EDU>

On Tue, 28 Feb 2006, Bill Shannon wrote:
> What
> should a server do if it gets flags in an APPEND command that it does
> not support?  At least one server is returning BAD in such a case.

A BAD is definitely wrong behavior.

A server should either:
  . create the flag(s) as needed
  . disregard the flag(s) and append anyway (perhaps issuing a warning in
    an untagged NO)
  . reject the command with a NO (not a BAD; there is no syntax error).

It may be reasonable for a STORE command to return a BAD for unknown flags 
if the server does not advertise \*.  But in the case of APPEND, the 
client has never been alerted with a FLAGS response as to what flags the 
mailbox offers (including \*) and thus BAD (which indicates syntax 
problems) is inappropriate.

Retry the command with only the predefined "system" flags.

> Should the client SELECT the destination mailbox, get the
> permanent flags, and perform an intersection between that set and the
> set in the source message and use that set for another APPEND command?

I think that is more work than is called for, all to deal with a broken 
server.

> Or should the client simply try the APPEND again with only the system
> flags, or no flags?

Retrying with just the system flags seems right.

And complain to the server vendor about its "BAD" behavior...

-- Mark --

http://staff.washington.edu/mrc
Science does not emerge from voting, party politics, or public debate.
Si vis pacem, para bellum.

