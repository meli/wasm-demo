MBOX-Line: From nicolson at google.com  Wed Jul 22 17:21:00 2015
To: imap-protocol@u.washington.edu
From: Jamie Nicolson <nicolson@google.com>
Date: Fri Jun  8 12:34:55 2018
Subject: [Imap-protocol] Strange behavior with Gmail IMAP when using
 CONDSTORE extension
In-Reply-To: <CAO3aFYuDxSFaVU_O8dVwpO_40_pdY3h-b2B8pPDyfm_=9MG1oQ@mail.gmail.com>
References: <CAO3aFYuDxSFaVU_O8dVwpO_40_pdY3h-b2B8pPDyfm_=9MG1oQ@mail.gmail.com>
Message-ID: <CACU8CfRygwG7dL4REXzUtGPYbR_P9ePGzrQkgZuJ1R8uQtd0mg@mail.gmail.com>

This could be a bug. I can't tell from this snippet, but what was the last
modification to happen to the message with UID 207722? Was it the delivery
of the message, marking it as read (STORE +FLAGS \Seen), or something else?

On Wed, Jul 22, 2015 at 4:54 PM, Michael Grinich <mgrinich@gmail.com> wrote:

> Our team has seen the following strange behavior when making requests to
> the Gmail IMAP server. I wanted to share this with the list and Brandon to
> see if folks have ideas for mitigating or fixing the issue, or whether this
> is actually a bug in Gmail's IMAP implementation.
>
> Here's the log:
>
> T2 SELECT "[Gmail]/All Mail"
> * FLAGS (\Answered \Flagged \Draft \Deleted \Seen $Forwarded $Junk
> $NotJunk $NotPhishing $Phishing NotJunk)
> * OK [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen $Forwarded
> $Junk $NotJunk $NotPhishing $Phishing NotJunk \*)] Flags permitted.
> * OK [UIDVALIDITY 11] UIDs valid.
> * 126069 EXISTS
> * 0 RECENT
> * OK [UIDNEXT 208199] Predicted next UID.
> * OK [HIGHESTMODSEQ 14064776]
> T2 OK [READ-WRITE] [Gmail]/All Mail selected. (Success)
> *T3 UID FETCH 207722 (FLAGS) (CHANGEDSINCE 14055062)*
> ** OK [HIGHESTMODSEQ 14064776]*
> ** 125605 FETCH (UID 207722 MODSEQ (14063567) FLAGS (\Answered \Seen))*
> T3 OK Success
> *T4 UID FETCH 1:* (FLAGS) (CHANGEDSINCE 14055062)*
> * 125938 FETCH (UID 208058 MODSEQ (14060020) FLAGS ())
> * 125939 FETCH (UID 208059 MODSEQ (14060011) FLAGS ())
> * 125940 FETCH (UID 208060 MODSEQ (14060028) FLAGS ())
> * 125941 FETCH (UID 208061 MODSEQ (14060000) FLAGS ())
> * 125942 FETCH (UID 208062 MODSEQ (14060036) FLAGS ())
> * 125943 FETCH (UID 208063 MODSEQ (14060000) FLAGS ())
> * 125944 FETCH (UID 208064 MODSEQ (14060000) FLAGS ())
> * 125945 FETCH (UID 208065 MODSEQ (14060003) FLAGS ())
> * 125946 FETCH (UID 208066 MODSEQ (14060037) FLAGS ())
> * 125947 FETCH (UID 208067 MODSEQ (14060043) FLAGS ())
> * 125948 FETCH (UID 208068 MODSEQ (14060000) FLAGS ())
> * 125949 FETCH (UID 208069 MODSEQ (14060045) FLAGS ())
> * 125950 FETCH (UID 208070 MODSEQ (14060000) FLAGS ())
> * 125951 FETCH (UID 208071 MODSEQ (14060025) FLAGS ())
> * 125952 FETCH (UID 208072 MODSEQ (14060011) FLAGS ())
> * 125953 FETCH (UID 208073 MODSEQ (14060045) FLAGS ())
> * 125954 FETCH (UID 208074 MODSEQ (14060044) FLAGS ())
> * 125955 FETCH (UID 208075 MODSEQ (14060003) FLAGS ())
> * 125956 FETCH (UID 208076 MODSEQ (14060040) FLAGS ())
> * 125957 FETCH (UID 208077 MODSEQ (14060011) FLAGS ())
> * 125958 FETCH (UID 208078 MODSEQ (14060044) FLAGS ())
> * 125959 FETCH (UID 208079 MODSEQ (14060025) FLAGS ())
> * 125960 FETCH (UID 208080 MODSEQ (14060044) FLAGS ())
> * 125961 FETCH (UID 208081 MODSEQ (14060036) FLAGS ())
> * 125962 FETCH (UID 208082 MODSEQ (14060037) FLAGS ())
> * 125963 FETCH (UID 208083 MODSEQ (14060003) FLAGS ())
> * 125964 FETCH (UID 208084 MODSEQ (14060037) FLAGS ())
> T4 OK Success
>
>
> Why would "UID FETCH 207722 (FLAGS) (CHANGEDSINCE 14055062)" return
> message 207722, but "UID FETCH 1:* (FLAGS) (CHANGEDSINCE 14055062)"
> wouldn't? Is there any way to catch all messages?
>
> I was able to reproduce this exact IMAP exchange multiple times, so
> there's no way the email could have been expunged within the two FETCHes.
>
> We've seen CONDSTORE-related issues happen on multiple Gmail accounts. The
> only workaround we found is "SEARCH MODSEQ", which didn't have this problem
> in tests. However, it was very slow on Gmail.
>
>
>
> Many thanks,
>
> -- Michael
>
> (I work at Nylas)
>
>
> _______________________________________________
> Imap-protocol mailing list
> Imap-protocol@u.washington.edu
> http://mailman13.u.washington.edu/mailman/listinfo/imap-protocol
>
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mailman13.u.washington.edu/pipermail/imap-protocol/attachments/20150722/6af22514/attachment.html>
