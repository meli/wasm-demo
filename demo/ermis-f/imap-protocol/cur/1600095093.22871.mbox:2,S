MBOX-Line: From tss at iki.fi  Sun Jun  1 13:21:37 2008
To: imap-protocol@u.washington.edu
From: Timo Sirainen <tss@iki.fi>
Date: Fri Jun  8 12:34:41 2018
Subject: [Imap-protocol] Proxy IP forwarding
In-Reply-To: <1070904325.21591212350911669.JavaMail.root@dogfood.zimbra.com>
References: <1070904325.21591212350911669.JavaMail.root@dogfood.zimbra.com>
Message-ID: <461F5E4A-6B12-4CAB-AFA6-D5CBABF0D7B6@iki.fi>

On Jun 1, 2008, at 11:08 PM, Dan Karp wrote:

>> The idea would be that there's a frontend IMAP proxy that looks up
>> the backend IMAP server based on the username, tells the backend
>> server the user's IP and then logs in using the provided user 
>> +password.
>
> We currently do this with a separate nginx process.  We tell the IMAP
> server the client's actual IP by having nginx do an
>
>   A001 ID ("X-ORIGINATING-IP" "<actual-client-IP>")
>
> as the first command, before authenticating.

That sounds interesting, although it kind of violates a MUST NOT in  
the ID spec:

> Implementations MUST NOT make operational changes based on the data  
> sent as part of the ID command or response. The ID command is for  
> human consumption only, and is not to be used in improving the  
> performance of clients or servers.

Especially if the IP is then used to do access checks. But since this  
is more of an internal proxy<->server action rather than client<- 
 >server, maybe it's not too bad to violate it. :) One good thing  
about it is that it could be sent automatically by simply checking if  
ID extension is supported by the destination server. My current code  
requires explicit configuration to enable sending the XFORWARD, which  
is just an annoying extra setting.

-------------- next part --------------
A non-text attachment was scrubbed...
Name: PGP.sig
Type: application/pgp-signature
Size: 201 bytes
Desc: This is a digitally signed message part
URL: <http://mailman13.u.washington.edu/pipermail/imap-protocol/attachments/20080601/de081d1f/attachment.sig>
