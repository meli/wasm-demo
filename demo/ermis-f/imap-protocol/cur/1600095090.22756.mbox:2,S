MBOX-Line: From mrc+imap at panda.com  Sun Nov 13 13:30:49 2011
To: imap-protocol@u.washington.edu
From: Mark Crispin <mrc+imap@panda.com>
Date: Fri Jun  8 12:34:47 2018
Subject: [Imap-protocol] [noob] multiple fetch responses for the same
	message?
In-Reply-To: <4EC02C84.1090209@gulbrandsen.priv.no>
References: <43724F01-C3E3-4039-8919-51E528C56C54@mac.com>
	<1321215266.30428.140660998383617@webmail.messagingengine.com>
	<755EBF71-00B6-4A4A-940A-CA8B5B62B10D@mac.com>
	<4EC02C84.1090209@gulbrandsen.priv.no>
Message-ID: <alpine.OSX.2.00.1111131306210.38075@hsinghsing.panda.com>

On Sun, 13 Nov 2011, Arnt Gulbrandsen wrote:
> It's safe for flags+rest. but not so safe for e.g. uid+rest:
> * 42 FETCH (UID 100)
> * 42 FETCH (RFC822.SIZE 12345)

Those pair responses is completely compliant. Any client which has the
slightest problem with it is broken.

Servers can do this. There are servers that do it.

The UID value does not identify the message to which it applies. The
message sequence number identifies the message.

Now, with that said, RFC 3501 says:
       The number after the "*" in an untagged FETCH response is always a
       message sequence number, not a unique identifier, even for a UID
       command response.  However, server implementations MUST implicitly
       include the UID message data item as part of any FETCH response
       caused by a UID command, regardless of whether a UID was specified
       as a message data item to the FETCH.

The second sentence seems to say that a server can't do:
 	* 42 FETCH (UID 100)
 	* 42 FETCH (RFC822.SIZE 12345)
in response to a UID FETCH n RFC822.SIZE command. That is a reasonable
interpretation of that statement.

HOWEVER!! Suppose the server did UID FETCH n ENVELOPE, and the server
decided to send the RFC822.SIZE as well. In that case, the FETCH response
with the RFC822.SIZE is (as far as the protocol is concerned) not "caused"
by the UID FETCH; and therefore that pair is alright.

This effectively means that a client MUST NOT assume that a UID will be
carried in a FETCH response; only that, somewhere in there, a UID will be
given. It must deal with message sequence number 42, of which UID 100 is a
property.

It's a difference that makes no real difference.

In other words even though it seems UID FETCH 100 RFC822.SIZE may not be
responded with:
 	* 42 FETCH (UID 100)
 	* 42 FETCH (RFC822.SIZE 12345)
the client must be able to handle that response any way. The server
certainly can do (albeit it would be pointless):
 	* 42 FETCH (UID 100 RFC822.SIZE 12345)
 	* 42 FETCH (RFC822.SIZE 12345)

The bottom line is that ANY untagged response is valid at ANY time. Client
implementations MUST be able to coalesce multiple untagged responses for
the same message based upon message sequence number.

It saves some bits on the wire if a server coalesces response data. It is
therefore very nice to have, and most servers do it. It is also absolutely
NOT a requirement that any server do so.

It may seem to be alright to write a client that ignores message sequence
numbers and only uses UIDs. A client author that does this deserves to
have his client break, and have his customers complain at him.

Server authors can already easily make your client break by violating
IMAP. Do you want to give them the power to make your client break by
obeying IMAP better than you do?

In conclusion:

Server authors should avoid doing things that are pointless and bandwidth
consuming.

Client authors MUST handle something that seems pointless from the server,
because some times a server implementation has no choice due to
circumstances out of its control.

-- Mark --

http://panda.com/mrc
Democracy is two wolves and a sheep deciding what to eat for lunch.
Liberty is a well-armed sheep contesting the vote.

