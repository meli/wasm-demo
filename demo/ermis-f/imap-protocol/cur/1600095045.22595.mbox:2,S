MBOX-Line: From tjs at psaux.com  Tue Mar 10 09:53:45 2015
To: imap-protocol@u.washington.edu
From: Tim Showalter <tjs@psaux.com>
Date: Fri Jun  8 12:34:54 2018
Subject: [Imap-protocol] If Crispin were creating IMAP today how would
	it be different?
In-Reply-To: <CAP1qinbr2hBTS1d_Sdeeqz9m37GXZgKzphEAx0J+SQYmbEr5zw@mail.gmail.com>
References: <54FAEB94.4070508@lavabitllc.com> <54FBF289.3010202@psaux.com>
	<7164.1425831184@parc.com>
	<1425907661.1215497.237833469.1EDA571D@webmail.messagingengine.com>
	<6506.1425915329@parc.com>
	<B03452330F6149E180E449A493F28C2B@gmail.com>
	<CAP1qinZdV1LW6XiWfqfk2A+TC6HsYsAWtT-KSffTNdOFqG_Tjw@mail.gmail.com>
	<80feadb5-24d0-4be9-a6d9-7885f422ad07@flaska.net>
	<CAP1qinbD3DAV0XMFCP3Y=C3Hpi8xWf50A8W7qZ5jDr0=Qa+jxQ@mail.gmail.com>
	<0D667625-94F0-4204-AC5A-7B6493496D3C@getboxer.com>
	<CAP1qinbr2hBTS1d_Sdeeqz9m37GXZgKzphEAx0J+SQYmbEr5zw@mail.gmail.com>
Message-ID: <F22E882E-0476-477E-9CB7-63121106217C@psaux.com>

> On Mar 10, 2015, at 9:29 AM, Imants Cekusins <imantc@gmail.com> wrote:
> 
> so, to summarize majority opinion:
> 
> IMAP & SMTP in their current state:
> - are efficient
> - lead to efficient hardware utilization
> - are simple enough for implementation
> 
> changes to the protocols are not justified
> 
> current selection of email server software offers enough choice. there
> is no real need for more alternatives

No, this is a poor summary.

IMAP and SMTP are constants of the ecosystem. They are problematic but a replacement is far more difficult.

The trade offs between binary and text protocols are complex, but unlikely to be the bottleneck for an I/O bound protocol. Parsing isn't what makes a mail server expensive.

Writing the parser is perhaps the easiest part of writing an email server, although writing an IMAP parser is quite tedious. Binary helps a little here but not enough to justify starting over.

Adding a new server to the set of IMAP, SMTP, and POP3 adds great expense since for the foreseeable future, we won't be able to stop using the old protocols.

Setting up Postfix and Dovecote is easier than writing email systems from scratch.

Tim
