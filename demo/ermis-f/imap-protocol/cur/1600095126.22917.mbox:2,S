MBOX-Line: From dave at cridland.net  Wed Apr 11 08:16:32 2007
To: imap-protocol@u.washington.edu
From: Dave Cridland <dave@cridland.net>
Date: Fri Jun  8 12:34:39 2018
Subject: [Imap-protocol] ACL: GETACL and "always granted" rights
In-Reply-To: <1998622862.168451176303885705.JavaMail.root@dogfood.liquidsys.com>
References: <1998622862.168451176303885705.JavaMail.root@dogfood.liquidsys.com>
Message-ID: <6701.1176304592.256996@peirce.dave.cridland.net>

On Wed Apr 11 16:04:45 2007, Dan Karp wrote:
> > > If a user has rights that "will always be granted" on a mailbox 
> > > (e.g. that show up in the first rights set in a LISTRIGHTS 
> untagged
> > > response), is that user/rights pair to be listed in the GETACL 
> > > response on that mailbox?
> > > I don't think you'd confuse clients by having a degree of 
> discrepency
> > between LISTRIGHTS and GETACL, but you might confuse clients by > 
> including entries in the ACL if they're not really needed. (For > 
> example, Thunderbird decides whether a mailbox is shared according 
> to
> > the contents of the ACL.)
> 
> OK.  So a reasonable implementation might, say, fail to include the 
> authenticated user's default rights on their own mailboxes?

Actually, you probably want to include that. I'd not be surprised if 
Thunderbird would get confused otherwise. It's other things you 
probably don't want to include, such as any rights that 
administrative users or system users have.

Arguably, you don't want those to be listed in LISTRIGHTS either.

Dave.
-- 
Dave Cridland - mailto:dave@cridland.net - xmpp:dwd@jabber.org
  - acap://acap.dave.cridland.net/byowner/user/dwd/bookmarks/
  - http://dave.cridland.net/
Infotrope Polymer - ACAP, IMAP, ESMTP, and Lemonade

