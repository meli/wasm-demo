MBOX-Line: From nevon_brake at consilient.com  Mon Apr 10 08:30:53 2006
To: imap-protocol@u.washington.edu
From: Nevon Brake <nevon_brake@consilient.com>
Date: Fri Jun  8 12:34:37 2018
Subject: [Imap-protocol] LIST Clarification
Message-ID: <443A7A2D.2070708@consilient.com>

I am seeking clarification on the following paragraph that appears in 
the LIST command section of RFC3501:

"The special name INBOX is included in the output from LIST, if INBOX is 
supported by this server for this user and if the uppercase string 
"INBOX" matches the interpreted reference and mailbox name arguments 
with wildcards as described above. The criteria for omitting INBOX is 
whether SELECT INBOX will return failure; it is not relevant whether the 
user's real INBOX resides on this or some other server."

 From this, can one conclude that a server implementation MAY not return 
the INBOX in a LIST such as:

C: a004 LIST "" Inbox
S: a004 OK LIST completed

when it does return it in a LIST such as:

C: a004 LIST "" INBOX
S: * LIST (\Unmarked) "/" INBOX
S: a004 OK LIST completed

Secondly, take a scenario where there are three mailboxes: INBOX, INBOX1 
and InBox1, and the client does a LIST:

C: a004 LIST "" In%B%

Assuming the implementation is case-sensitive, what must the server 
return? More importantly, can it NOT return the INBOX? I realize this 
example is a bit contrived, but it may help in the clarification.

Thank you,

Nevon

