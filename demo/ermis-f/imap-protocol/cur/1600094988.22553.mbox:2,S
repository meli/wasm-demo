MBOX-Line: From David.Harris at pmail.gen.nz  Sat Feb 18 15:12:42 2017
To: imap-protocol@u.washington.edu
From: David Harris <David.Harris@pmail.gen.nz>
Date: Fri Jun  8 12:34:55 2018
Subject: [Imap-protocol] Is SELECT required before FETCH to detect new
	mail?
In-Reply-To: <3e01f804-163d-cda3-f5bb-b495fa5a1d50@chartertn.net>
References: <bb8d0ae1-dda0-dbce-186a-2a668946ec7b@chartertn.net>,
	<CABa8R6tLdckvJ9c-UHHijbvJ95fBGBN41fxhypO1yWzt0tGOMQ@mail.gmail.com>,
	<3e01f804-163d-cda3-f5bb-b495fa5a1d50@chartertn.net>
Message-ID: <58A8D4EA.24417.4193CD74@David.Harris.pmail.gen.nz>

On 18 Feb 2017 at 17:29, Gene Smith wrote:

> The charter IMAP server is openwave according the login response. So
> are you saying it is OK for the IMAP server to require a new SELECT
> before doing a FETCH on an already selected mailbox for the FETCH
> response to indicate new email? If so, this is a bug in thunderbird. 

One of the major reasons IMAP has such detailed provision for unsolicited 
responses is so that the server can report the arrival of new messages in a 
mailbox *without* the client having to re-select it. Depending on the back-end, 
SELECT can be a fantastically "expensive" operation, and even on servers 
where it's well-optimized, it's probably going to incur significant overhead.

Speaking without any pretence of being authoritative, I'd say that if the only way 
the server can report new messages in a mailbox is through reselection, then it's 
broken, if only because there's no way for a client to learn procedurally that 
that's what the server wants.

In my own client code, I issue periodic NOOP commands and expect to see new 
messages reported as part of the response sequence to that command: that's 
how I would have expected most clients would do it.

Cheers!

-- David --

------------------ David Harris -+- Pegasus Mail ----------------------
Box 5451, Dunedin, New Zealand | e-mail: David.Harris@pmail.gen.nz
           Phone: +64 3 453-6880 | Fax: +64 3 453-6612

Real newspaper headlines from US Papers:
   "Thieves steal burglar alarm".



