MBOX-Line: From mrc at CAC.Washington.EDU  Mon Nov 19 10:59:27 2007
To: imap-protocol@u.washington.edu
From: Mark Crispin <mrc@CAC.Washington.EDU>
Date: Fri Jun  8 12:34:40 2018
Subject: [Imap-protocol] IMAP4 section numbering algorithm
In-Reply-To: <F27BF3CBACCE8B4BB17554ABA0AB21888172FD@ILEXC3U03.ndc.lucent.com>
References: <F27BF3CBACCE8B4BB17554ABA0AB21888172FD@ILEXC3U03.ndc.lucent.com>
Message-ID: <alpine.OSX.0.99999.0711191043510.14842@pangtzu.panda.com>

On Mon, 19 Nov 2007, Esh, Thomas D (Tom) wrote:
> Where would I find a document that describes how IMAP4 assigns section 
> numbers to MIME msgs?  I might be missing something because it doesn't 
> seem obvious to me.

I assume that you have read the text in RFC 3501 section 6.4.5, 
particularly on pages 55 and 56, and are still scratching your head.  What 
you need is there, but it often requires multiple re-readings.

In the name of "a picture is worth 1000 words", take a look at the 
infamous MIME Torture Test message at:
 	ftp://ftp.cac.washington.edu/mail/mime-examples/torture-test.mbox
It's horrible, and few MIME parsers pass it the first time.

That message has a body structure numbering regime looking like:
  1 TEXT/PLAIN (Explanation);CHARSET=US-ASCII (3 lines)
  2 MESSAGE/RFC822 (Rich Text demo) (106 lines)
  2.1 TEXT/PLAIN;CHARSET=US-ASCII (16 lines)
  2.2.1 TEXT/RICHTEXT;CHARSET=US-ASCII (13 lines)
  2.3 APPLICATION/ANDREW-INSET (917 bytes)
  3 MESSAGE/RFC822 (Voice Mail demo) (7605 lines)
  3.1 AUDIO/BASIC (Hi Mark) (561308 bytes)
  4 AUDIO/BASIC (Flint phone) (36234 bytes)
  5 IMAGE/PBM (MTR's photo) (1814 bytes)
  6 MESSAGE/RFC822 (Star Trek Party) (4565 lines)
  6.1.1 TEXT/PLAIN;CHARSET=US-ASCII (16 lines)
  6.1.2 AUDIO/X-SUN (He's dead, Jim) (31472 bytes)
  6.2.1 IMAGE/GIF (Kirk/Spock/McCoy) (26000 bytes)
  6.2.2 IMAGE/GIF (Star Trek Next Generation) (18666 bytes)
  6.2.3 APPLICATION/X-BE2;VERSION=12 (46125 bytes)
  6.2.4 APPLICATION/ATOMICMAIL;VERSION=1.12 (9203 bytes)
  6.3 AUDIO/X-SUN (Distress calls) (47822 bytes)
  7 MESSAGE/RFC822 (Digitizer test) (483 lines)
  7.1 TEXT/PLAIN;CHARSET=US-ASCII (0 lines)
  7.2 IMAGE/PGM (Bellcore mug) (84174 bytes)
  7.3 TEXT/PLAIN;CHARSET=US-ASCII (8 lines)
  8 MESSAGE/RFC822 (More Imagery) (431 lines)
  8.1 TEXT/PLAIN;CHARSET=US-ASCII (26 lines)
  8.2 IMAGE/PBM (Mail architecture slide) (71686 bytes)
  9 MESSAGE/RFC822 (PostScript demo) (6438 lines)
  9.1 APPLICATION/POSTSCRIPT (Captain Picard) (397154 bytes)
  10 IMAGE/GIF (Quoted-Printable test) (78302 bytes)
  11 MESSAGE/RFC822 (q-p vs. base64 test) (1382 lines)
  11.1 AUDIO/BASIC (I'm sorry, Dave (q-p)) (62094 bytes)
  11.2 AUDIO/BASIC (I'm sorry, Dave (BASE64)) (40634 bytes)
  12 MESSAGE/RFC822 (Multiple encapsulation) (3282 lines)
  12.1 APPLICATION/POSTSCRIPT (The Simpsons!!) (53346 bytes)
  12.2 BINARY/UNKNOWN (Alice's PDP-10 w/ TECO & DDT);NAME=Alices_PDP-10 (18530 bytes)
  12.3 MESSAGE/RFC822 (Going deeper) (2094 lines)
  12.3.1 TEXT/PLAIN;CHARSET=US-ASCII (7 lines)
  12.3.2.1 IMAGE/GIF (Bunny) (3276 bytes)
  12.3.2.2 AUDIO/BASIC (TV Theme songs) (156706 bytes)
  12.3.3 APPLICATION/ATOMICMAIL (4924 bytes)
  12.3.4 MESSAGE/RFC822 (Yet another level deeper...) (1031 lines)
  12.3.4.1 AUDIO/X-SUN (I'm Twying...) (75682 bytes)

Attached is telemetry showing what UW imapd will do in that message.

Your implementation should do something essentially similar.

For client code (assuming that you are not running in a "clean-room" 
development environment where you're forbidden from looking at other 
people's code), the UW IMAP toolkit includes a very basic demonstration 
program called mtest, and in mtest.c there is a routine called 
display_body() that runs down the body structure recursively.  Many people 
have found it quite instructive in understanding how IMAP's representation 
of MIME structure works.

It is one of these lightbulb type of experiences, where you muddle around 
for a while, then suddenly it all becomes obvious and you can't imagine it 
being any other way.

Good luck, and if there's still some shading on the lightbulb feel free to 
ask me further questions.

-- Mark --

http://panda.com/mrc
Democracy is two wolves and a sheep deciding what to eat for lunch.
Liberty is a well-armed sheep contesting the vote.
-------------- next part --------------
A non-text attachment was scrubbed...
Name: uw.telemetry
Type: application/octet-stream
Size: 8444 bytes
Desc: UW IMAP telemetry of BODYSTRUCTURE fetching
URL: <http://mailman13.u.washington.edu/pipermail/imap-protocol/attachments/20071119/a3763c4d/attachment.obj>
