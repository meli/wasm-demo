From: hinsen at cnrs-orleans.fr (Konrad Hinsen)
Date: 09 Apr 1999 20:10:52 +0200
Subject: Numeric: vector operations?
References: <joe-090419991023459248@chinacat.salk.edu>
Message-ID: <m34smpu0k3.fsf@chinon.cnrs-orleans.fr>
Content-Length: 1517
X-UID: 385                                                  

Joe Strout <joe at strout.net> writes:

> Playing with Numeric today, I noticed an apparent lack of common vector
> operations, like normalization, vector-length, and cross product. 

If you mean vectors as in 3D geometry, look at the module Vector in my
collection of scientific modules
(http://starship.python.net/crew/hinsen/scientific.html).

For vectors in the sense of rank-one arrays, I am not aware of any
available module that contains these operations (and the cross product
is limited to 3D-space anyway), but they are also much less frequently
needed for non-geometric work.

> These are fairly easy to code for my own evil purposes, but it's
> surprising that they're not in there.  Am I missing something?  Or are
> they purposefully omitted for some reason (e.g., they only apply to
> very restricted sorts of matrices whereas other operations are more
> general)?

I guess there is too little advantage in writing
   Numeric.vector_length(v)
over
   Numeric.sqrt(Numeric.add.reduce(v**2))

-- 
-------------------------------------------------------------------------------
Konrad Hinsen                            | E-Mail: hinsen at cnrs-orleans.fr
Centre de Biophysique Moleculaire (CNRS) | Tel.: +33-2.38.25.55.69
Rue Charles Sadron                       | Fax:  +33-2.38.63.15.17
45071 Orleans Cedex 2                    | Deutsch/Esperanto/English/
France                                   | Nederlands/Francais
-------------------------------------------------------------------------------




