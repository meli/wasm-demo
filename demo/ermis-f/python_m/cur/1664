From: achrist at easystreet.com (Al Christians)
Date: Fri, 02 Apr 1999 14:28:32 -0800
Subject: string.join() vs % and + operators
Message-ID: <37054490.E79ADCC9@easystreet.com>
Content-Length: 1668
X-UID: 1664                                                  

Whereas Python's strings are immutable, there is potentially a strong
incentive to get them right the first time.  In my applications, I
want to create strings that have many fields within them.  I assume that
it would be nice to be able to modify the fields without creating a new
string any time its contents get changed, but I don't think that Python
gives any nice way to do this.  So, I'm stuck with building the string
from many little pieces.  The 'many' part of this gives me some worry
about efficiency, which it is better not to worry about, so I did a 
brief test to see if there is ugly downside to this.  I ran the
following script:

#  Start of Script
import string
# Create an array of little strings and a format to join them
s = []
f = ''
for i in range(100):
	s.append(`i`)
	f = f + '%s'	

print "Start of Way 1 -- Create with a big format"
for i in range(100000):
	z = f % tuple(s)
print "end of Way 1"
print z
raw_input()
print "Start of Way 2 -- Create with a join"
for i in range(100000):
	z = string.join(s, '')
print "End of Way 2"
print z
raw_input()
print "Start of Way 3"
for i in range(100000):
    z = ''
    for j in s:
	    z = z + j
print "End of Way 3"
print z

#  End of Script

This ran amazingly fast on my Pentium 200 Mhz -- around 11 seconds for
Way 1, and 7 for Way 2.  So, either way, Python can put together about
1 million little strings in a second.  Way 3, the way that one would 
expect to be bad, recreating the string with each concatenation, was
much slower, but only took about 1 minute.  Surprisingly swift as well.

Anybody have anything to add to this?  Are there any related pitfalls
that I may have missed?

Al




