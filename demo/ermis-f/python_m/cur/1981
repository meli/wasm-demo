From: joe at strout.net (Joe Strout)
Date: Tue, 11 May 1999 13:25:42 -0700
Subject: my loop is too slow
References: <Pine.SGI.3.91.990510155017.9052A-100000@dft>
Message-ID: <joe-110519991325429671@chinacat.salk.edu>
Content-Length: 1808
X-UID: 1981                                                 

In article <Pine.SGI.3.91.990510155017.9052A-100000 at dft>, SC Zhong
<sai at dft.chem.cuhk.edu.hk> wrote:

> I tried to learn and use python a week ago, and wrote
> a small application.  When it run the loop:
> 
>         for k in range(dimension):
>                 for i in range(dimension):
>                         for j in range(dimension):
>                                 for h in range(dimension):
>                                         a[k,i]=a[k,i]+c[k,h]*c[k,j]*\
>                                                 f[j,i]*f[i,h]
> 
> it took very long time (may be 10 hours in my HP workstation). dimension
> is 142 and a,c,f are all complex.  

There are several things you can do.

1. Change your loop structure.  Compute partial results as much as
possible.  E.g., part of your product is c[k,j]*f[j,i]; this does not
change inside the j loop, so you should not be recomputing it on every
iteration of the 'h' loop.

2. If you're doing something that can be expressed in linear algebra,
consider using the Numeric module; it is MUCH faster than standard
Python at such operations.

3. You could write just the number-crunching routine in C or FORTRAN or
whatever, as a Python extension.  This is a very powerful technique.

I'm still amazed that it took 10 hours for this.  That seems unusually
slow, though a 4D array does get rather large... about half a million
elements.  Could it be you were short on RAM, and swapping to disk?

-- 
,------------------------------------------------------------------.
|    Joseph J. Strout           Biocomputing -- The Salk Institute |
|    joe at strout.net             http://www.strout.net              |
`------------------------------------------------------------------'
Check out the Mac Web Directory!    http://www.strout.net/macweb.cgi




