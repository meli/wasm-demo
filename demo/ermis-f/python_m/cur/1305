From: wtanksle at dolphin.openprojects.net (William Tanksley)
Date: Thu, 29 Apr 1999 23:18:25 GMT
Subject: Dangers of C++ (Way off topic)
References: <613145F79272D211914B0020AFF6401914DAD8@gandalf.digicool.com> 	<p5g15lmb35.fsf@bidra241.bbn.hp.com> <slrn7ieipq.8uk.wtanksle@dolphin.openprojects.net> <372769B0.3CE8C0F3@prescod.net> <slrn7if4d4.9lh.wtanksle@dolphin.openprojects.net> <372827D8.7A5F@mailserver.hursley.ibm.com>
Message-ID: <slrn7ihq61.dk7.wtanksle@dolphin.openprojects.net>
Content-Length: 2013
X-UID: 1305                                                  

On Thu, 29 Apr 1999 10:35:20 +0100, Paul Duffin wrote:
>William Tanksley wrote:

>> Let me expand a little on the dangers of C++.

>>  - virtual functions.  It shouldn't be my job to tell the compiler when
>> virtual lookups are the only thing to do; the compiler ought to be able to
>> tell.  For performance gurus, a way to hint to the compiler that virtual
>> lookups were _not_ needed might be useful -- but what if the programmer
>> was wrong?

>How can the compiler tell ?

Because there's only one possibility in all of the libraries with which
the system is linked.  Or perhaps it can tell in some other way -- perhaps
the object to which the message is being sent was just declared and
couldn't have changed.

But let's suppose you have the stupidest compiler in the world (i.e. one
that never makes optimizations).  What's the _worst_ thing that could
happen if functions defaulted to virtual? Your code would be a tiny bit
slower.  Yawn.

But with only a little more cleverness in the compiler, you wind up with
the compiler able to make virtual calls when virtual calls are needed, and
direct ones otherwise.  This means that the same function could be called
virtually one moment and directly the next.

>>  - inline functions.  Again, a good compiler HAS to make this decision for
>> itself, and in a good compiler, whether or not this decision was made
>> should be transparent to the programmer.

>A good compiler will, inline is only a hint to the compiler, the compiler
>can choose whether or not to inline that function, it can also choose to
>inline other functions which have not been marked as inline.

Good.  So get rid of it.

Oh, and even a good C++ compiler can't choose to inline a function which
wasn't defined in the header file.  And at that, it doesn't have complete
information about what the global impact of inlining will be -- only for
that one module.

>Paul Duffin

-- 
-William "Billy" Tanksley
"But you shall not escape my iambics."
           -- Gaius Valerius Catullus




