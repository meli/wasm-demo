From: tismer at appliedbiometrics.com (Christian Tismer)
Date: Mon, 19 Apr 1999 18:00:12 GMT
Subject: Memory and swapping question
References: <371B5ED8.A9C82170@appliedbiometrics.com> <199904191731.NAA03862@eric.cnri.reston.va.us>
Message-ID: <371B6F2C.D48C63BE@appliedbiometrics.com>
Content-Length: 1892
X-UID: 805                                                  

Guido van Rossum wrote:
...
> > >>> x=range(big)
> > >>> del x
> > >>>
> >
> > On my system, creation takes about 10 times as for big/2,
> > this is ok. But the del takes at least three times as long.
> > Besides the fact that integers are never really disposed but
> > build up a freelist, why is deletion so much slower now?
> 
> Clearly in the second case you're exceeding your physical memory and
> paging VM pages in and out of swap space?

Yes, this was what I wanted to check out.

> My guess: when creating the list you are allocating new VM pages,
> which don't require any overhead until they need to be written, but
> when deleting it, each page gets read from swap space, modified, and
> then written back.  Thus, you're I/O bound, and deleting requires more
> I/O.

Now I've got it. Right, since the structures almost fit main memory,
there is a little but not too much swapping, just pushing other
processes memory away.
Then, all the integers are deallocated which means they are
not deleted at all, but written again since they build up
the free list. This ensures that my whole memory gets written
to the disk. The same would happen if I'd read once through the
list. 
But, not really. Doesn't this suggest to do memory deallocation
from the end to the start of a list? I could imagine that the
probability to touch something in memory is higher in this
case. Did someone try this before? (before I waste time)

thanks & cheers - chris

-- 
Christian Tismer             :^)   <mailto:tismer at appliedbiometrics.com>
Applied Biometrics GmbH      :     Have a break! Take a ride on Python's
Kaiserin-Augusta-Allee 101   :    *Starship* http://starship.python.net
10553 Berlin                 :     PGP key -> http://wwwkeys.pgp.net
PGP Fingerprint       E182 71C7 1A9D 66E9 9D15  D3CC D4D7 93E2 1FAE F6DF
     we're tired of banana software - shipped green, ripens at home




