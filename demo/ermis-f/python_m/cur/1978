From: arcege at shore.net (Michael P. Reilly)
Date: Tue, 11 May 1999 16:57:13 GMT
Subject: ExpectPy 1.8 - Expect extension for Python
Message-ID: <JjZZ2.1402$9L5.517074@news.shore.net>
Content-Length: 3206
X-UID: 1978                                                 

[ This is a repost of the following article:                               ]
[ From: "Michael P. Reilly" <arcege at shore.net>                             ]
[ Subject: ExpectPy 1.8 - Expect extension for Python                      ]
[ Newsgroups: comp.lang.python.announce                                    ]
[ Message-ID: <mt2.0-32064-926429940 at news.informatik.uni-bonn.de>          ]

[This was originally posted April 26th, but didn't get to the moderator.]

I've finally gotten time to work on the next release of ExpectPy; the
release is also delayed so I could test against Python 1.5.2.  The
major change is the addition of a simple interact function (and method)
which links the I/O of two data streams (sockets, file, spawned
objects, etc.).  There is also a new interface for accessing the Expect
variables and a stty method.

The documentation is bundled alongside the extension tarfile.  I'll be
making Python-standard (LaTeX based) docs soon.

The new version replaces the old at the URL
  http://www.shore.net/~arcege/python/ExpectPy/.
My address is arcege at shore.net.  I'm sure there will be some bugs (I
just found a minor one this morning and have already documented it);
please let me know what they are.

I have been asked if ExpectPy runs on NT.  There is no official,
supported version of Expect on NT, but someone has ported it;  I have
no plans to port ExpectPy to a NT release.  I'm neither a NT
programmer, nor (as a release manager) do I like the idea of making an
unofficial port on top of an unofficial port.

  -Arcege

Changes since 1.7.1:
* Move shared declarations into header files (a sign that the module is
  getting larger).
* Fix a number of pathname problems within the configure.in and
  Makefile.in files.
* Allow --with-python=<dir> to specify a build directory instead of an
  installation.
* Add a rudimentary version of Expect's interact function.  This is
  added as a module function and a method to spawn and spawnfile
  objects.
* The module is now "Python-thread safe".
* Add new interface to Expect variables: settings.  The old function
  "set" still exists for backward compatibility.
* Add a method to allow changes to terminal parameters (baudrate, raw,
  no-echo).
* Use patchlevel.h protocol for version number changes.
* Have configure better determine what can be built.
* Bring documentation up to date and fix some typos.
* Add more __doc__ strings.

<P><A HREF="http://www.shore.net/~arcege/python/ExpectPy/">ExpectPy
1.8</A> - Python adaptation of Don Libes's "Expect" library for automation
of interactive UNIX processes. (10-May-99)

== 
------------------------------------------------------------------------
| Michael P. Reilly, Release Engineer | Email: arcege at shore.net        |
| Salem, Mass. USA  01970             |                                |
------------------------------------------------------------------------

-- 
----------- comp.lang.python.announce (moderated) ----------
Article Submission Address:  python-announce at python.org
Python Language Home Page:   http://www.python.org/
Python Quick Help Index:     http://www.python.org/Help.html
------------------------------------------------------------




