From: wtanksle at dolphin.openprojects.net (William Tanksley)
Date: Sun, 02 May 1999 20:14:22 GMT
Subject: Emulating C++ coding style
References: <371F8FB7.92CE674F@pk.highway.ne.jp> <slrn7hvbbu.bmd.wtanksle@dolphin.openprojects.net> <3720675C.FF7A9E99@gssec.bt.co.uk> <slrn7i7r00.10d.wtanksle@dolphin.openprojects.net> <3727DE55.EA5B4BAF@palladion.com>
Message-ID: <slrn7ipcgt.boj.wtanksle@dolphin.openprojects.net>
Content-Length: 1939
X-UID: 1921                                                 

On Wed, 28 Apr 1999 23:21:41 -0500, Tres Seaver wrote:
>William Tanksley wrote:

>> On Fri, 23 Apr 1999 13:28:12 +0100, Alan Gauld wrote:

>> >> In proper C++, your const will be contained inside a class (probably
>> >> static) to keep it out of the global namespace.

>> >Hopefully not. It should be contained within a C++ namespace.

>> Okay, I'll bite.  Why "hopefully not" a class?  I know namespaces are new
>> and cool, but classes seem to have done the job very well in the past.
>> Have they been secretly causing bloat in our code all along ;-)?

>Classes are semantically "heavier" than namespaces:  by design, they exist to
>classify objects.  Classes which exist only to scope other names (typedefs,
>constants, free functions), while a decent workaround before the ANSI standard,
>are now less attractive (they can't be reopened, for one thing).

Now that makes a lot of sense.

However, investigating my code for compliance with this statement, I find
that every time I've declared consts or enums it's always been part of a
class's state.  So I can't just move it out into a namespace without
having it look odd.

In the future, though, my design will include the use of namespaces for
inherited state enumerations (i.e. ones which descendant classes may be
expected to use).

What does THIS, you ask, have to do with Python?  Well, it serves to
reiterate my point: don't break a cool feature by not planning it out
completely.  C++ broke a LOT of things that way.

Speaking of which, I'm kind of wondering how to take the address of a
function in C++ in the presence of overloading.  In C it was trivial, but
in C++ you can't distinguish between different functions based only on
names.  Odd...  I can't find anything which mentions how to do that.

>Tres Seaver         tseaver at palladion.com    713-523-6582

-- 
-William "Billy" Tanksley
"But you shall not escape my iambics."
           -- Gaius Valerius Catullus




