From: jschiotz at hotmail.com (Jakob Schiotz)
Date: Tue, 27 Apr 1999 14:39:36 GMT
Subject: WARNING: AIX and dynamic loading.
Message-ID: <7g4i77$qif$1@nnrp1.dejanews.com>
Content-Length: 2475
X-UID: 306                                                  


Hi everybody,

I would like to warn developers using AIX against this trap waiting
for us to fall into.  (I am cross-posting this to the SWIG mailing list
although it is not strictly a SWIG problems, as SWIG users will be
doing just the kind of stuff that gets you into trouble).

SCENARIO:
You are developing a dynamically loaded C module for Python (or
possibly for other languages).  You place the module in a directory on
the PYTHONPATH.  That directory is NFS mounted.  Or you are loading
the module directly from the directory where you compiled it, and that
directory is NFS mounted.

SYMPTOMS:
You are improving the code, but it does not seem to have any effect at
all.  Eventually you realise that Python is loading the buggy version
of your module that you tried out this morning, although _all_ copies
on disk are the new improved version you just wrote !

PROBLEM:
If you copy the module to the PYTHONPATH directory using cp, the old
file gets overwritten, but keeps the same inode number.  Apparently,
the AIX dynamical loader is caching the module somewhere, and does not
discover that the file has been modified. (If the directory is not NFS
mounted cp will fail with an error message saying that you cannot
overwrite a running program - although the program has stopped.)

It is _possible_ that this only occurs if the module you wrote causes
python to dump core.  It certainly makes it frustrating to fix the
bug as you continue to get the version that dumps core loaded into
python, even after you fixed the bug.

SOLUTION:
You makefile should remove the old module (the .so file) before copying the
new version into the installation directory.  Then the file gets a new
inode number and the loader discovers that it has been changed.  If
you ever load the module directly from the development directory you
should also remove the .so file before compiling/linking, as you will
otherwise get hit by the same bug.


I just wanted to warn you all about this bug in AIX.  I wasted too much time
on this last Friday :-(

Best regards,

Jakob Schiotz

--
Jakob Schiotz, CAMP and Department of Physics, Tech. Univ. of Denmark,
DK-2800 Lyngby, Denmark.  http://www.fysik.dtu.dk/~schiotz/
This email address is used for newsgroups and mailing lists
(spam protection).  Official email: schiotz @ fysik . dtu . dk

-----------== Posted via Deja News, The Discussion Network ==----------
http://www.dejanews.com/       Search, Read, Discuss, or Start Your Own    




