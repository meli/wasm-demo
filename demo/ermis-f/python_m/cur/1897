From: da at ski.org (David Ascher)
Date: Fri, 21 May 1999 12:15:31 -0700 (Pacific Daylight Time)
Subject: Defining VCL-like framework for Python
In-Reply-To: <l0310280db36b5c8fab56@[193.78.237.148]>
Message-ID: <Pine.WNT.4.04.9905211159280.289-100000@rigoletto.ski.org>
Content-Length: 1617
X-UID: 1897                                                 

> Do you happen to know where I could read what "environmental acquisition"
> is? Or, if you have a spare minute and the concept happens to be
> explainable in a sentence or two...

There's a URL, but it's gone (temporarily, I'm sure).  Jim gave a talk at
the DC FedUnix IPC on it, there might be something on www.python.org... .
(can't find anything detailed).

The idea is that one can set things up so that instances get attributes
from their parents in a containment (has-a) hierarchy, not just the
inheritance (is-a) hierarchy.  This makes sense for things like background
colors in GUI elements, for example -- the color of a widget should by
default be the color of the parent widget.  That way you change the color
of the toplevel window, and all subwidgets get that color, except if they
override it "locally".

Jim has some tools which use ExtensionClasses to make that happen:

>>> class Window(Acquisition.Implicit):
...    background = 'white'
...    def __init__(self):
...       self.menubar = MenuBar()
...
>>> class MenuBar(Acquisition.Implicit):
...   def report(self):
...     print "self.background =", self.background
...
>>> w = Window()
>>> w.menubar.report()
self.background = white

It's very cool, somewhat non-trivial to use right (it affects the way one
designs massively), and requires a fairly deep brain-tweak. Still, worth
knowing about when designing a new framework where containment is such an
important notion.  It's one of the few "new" concepts in programming that
I've seen in the last few years which I think has huge potential for
simplifying complex designs.

--david








